﻿using System;
using System.ComponentModel.Composition;
using System.Xml.Serialization.Configuration;
using Emc.ClientCommons.ViewModels;
using Emc.ClientContracts.ViewModels.Registrations;

namespace EMedic.Wpf.Registrations
{
    [Export(typeof(IPatientViewModel))]
    public class PatientViewModel: BaseFormViewModel, IPatientViewModel
    {
        private static readonly PatientVidator Validator = new PatientVidator();
        public override ValidationResult Validate()
        {
            return RootedPathValidator.a
        }
    }
}
