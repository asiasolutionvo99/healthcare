﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Caliburn.Micro;
using DevExpress.Xpf.Core;
using Emc.ClientBusinessLayers.Validators.Registration;
using Emc.ClientBusinessLayers.Workflows;
using Emc.ClientCommons;
using Emc.ClientCommons.ViewModels;
using Emc.ClientContracts.ViewModels.Registrations;
using Emc.Contracts.DataTransferObjects;
using Emc.Domains;

namespace EMedic.Wpf.Registrations.ViewModels
{
    [Export(typeof(IPatientViewModel)),PartCreationPolicy(CreationPolicy.NonShared)]
    public class PatientViewModel:BaseFormViewModel, IPatientViewModel
    {
        private static readonly PatientValidation patientValidator = new PatientValidation();
        private string _patientName;
        private string _dob;
        private List<Country> _countries;
        private Country _country;
        private List<CityProvince> _provinces;
        private CityProvince _province;

        private List<Suburb> _suburbs;
        private List<Suburb> _refSuburbs;
        private Patient _patient;

        public Patient Patient { get; set; }

        public List<Suburb> RefSuburbs
        {
            get { return _refSuburbs; }
            set
            {
                _refSuburbs = value;
                NotifyOfPropertyChange(()=>RefSuburbs);
            }
        }

        public Country SelectedCountry { get; set; }
        public CityProvince SelectedProvince { get; set; }
        public Suburb SelectedSuburb { get; set; }

        public PatientViewModel()
        {
            Patient = new Patient();
        }

        public void initdata()
        {
            GetAllCountry();
            GetAllcityprovince();
            //this.NotifyModelChanged(Patient);
            //GetAllsuburb();
        }

        public override FluentValidation.Results.ValidationResult Validate()
        {
            return patientValidator.Validate(this);
        }

        public string PatientName
        {
            get { return _patientName; }
            set
            {
                _patientName = value;
                NotifyOfPropertyChange(()=> PatientName);
            }
        }

        public List<CityProvince > Cityprovince 
        {
            get { return _provinces ; }
            set
            {
                _provinces  = value;
                NotifyOfPropertyChange(() => _provinces );
            }
        }

        public List<Suburb > Suburbs 
        {
            get { return _suburbs ; }
            set
            {
                _suburbs  = value;
                NotifyOfPropertyChange(() => _provinces);
            }
        }


        public List<Country> Countries
        {
            get { return _countries; }
            set
            {
                _countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

      

        public Country Country
        {
            get { return _country; }
            set
            {
                _country = value;
                NotifyOfPropertyChange(() => Country);
            }
        }

        public string Dob
        {
            get { return _dob; }
            set
            {
                _dob = value;
                NotifyOfPropertyChange(() => Dob);
            }
        }

        #region function

        protected override void OnActivate()
        {
            base.OnActivate();
            initdata();
        }

        private void GetAllCountry()
        {
            var workflow = ConfigurationWorkflow.GetAllCountry(this);
            Coroutine.BeginExecute(workflow.GetEnumerator());
        }
        private void GetAllcityprovince()
        {
            var workflow = ConfigurationWorkflow.GetAllprovine(this);
            Coroutine.BeginExecute(workflow.GetEnumerator());
        }

        //private void GetAllsuburb()
        //{
        //    var workflow = ConfigurationWorkflow.GetAllsuburb(this);
        //    Coroutine.BeginExecute(workflow.GetEnumerator());
        //}

        public void BtnSearch()
        {
            ValidationEnabled = true;//clear set to false
            if (IsValid)
            {
                Patient.PatientFields = new List<PatientField>();
                Patient.PatientFields.Add( new PatientField {FieldId = 1, FieldValue = Convert.ToString( SelectedCountry.Id)} );
                Patient.PatientFields.Add(new PatientField { FieldId = 2, FieldValue = Convert.ToString(SelectedProvince.Id) });
                Patient.PatientFields.Add(new PatientField { FieldId = 3, FieldValue = Convert.ToString(SelectedSuburb .Id) });
                
                var patientSave = ConfigurationWorkflow.SavePatient(Patient, this);
                Coroutine.BeginExecute(patientSave.GetEnumerator());
            }
        }

        #endregion
    }
}
