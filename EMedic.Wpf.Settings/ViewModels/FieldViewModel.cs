﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using Emc.ClientBusinessLayers.Validators;
using Emc.ClientBusinessLayers.Workflows;
using Emc.ClientCommons;
using Emc.ClientCommons.ViewModels;
using Emc.ClientContracts.ViewModels;
using Emc.ClientContracts.ViewModels.Settings;
using Emc.Domains;
using FluentValidation.Results;

namespace EMedic.Wpf.Settings.ViewModels
{
    [Export(typeof(IFieldViewModel))]
    public class FieldViewModel : BaseFormViewModel, IFieldViewModel
    {
        private static readonly FieldValidator Validator = new FieldValidator();
        private string _fieldName;
        private string _dataType;

        public FieldViewModel()
        {
            
        }

        public long Id { get; set; }

        public string FieldName
        {
            get { return _fieldName; }
            set
            {
                _fieldName = value;
                NotifyOfPropertyChange(() => FieldName);
            }
        }

        public string DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                NotifyOfPropertyChange(() => DataType);
            }
        }

        public void SaveCmd()
        {
            this.ValidationEnabled = true;
            if (IsValid)
            {
                var field = new Field { Id = Id, FieldName = FieldName, DataType = DataType};
                AsyncActions.CoroutineExecute( SettingWorkflow.SaveField(field));
            }
            else
            {
                Globals.ShowMessage(" Thông tin chưa hợp lệ ");
            }
        }

        public void CloseCmd()
        {
            this.TryClose();
        }

        public override ValidationResult Validate()
        {
            return Validator.Validate(this);
        }

    }
}
