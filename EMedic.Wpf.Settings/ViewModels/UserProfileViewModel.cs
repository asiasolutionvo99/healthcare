﻿using System.ComponentModel.Composition;
using Emc.ClientBusinessLayers.Validators;
using Emc.ClientBusinessLayers.Workflows;
using Emc.ClientCommons;
using Emc.ClientCommons.ViewModels;
using Emc.ClientContracts.ViewModels.Settings;

namespace EMedic.Wpf.Settings.ViewModels
{
    [Export(typeof(IUserProfileViewModel)), PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserProfileViewModel : BaseFormViewModel, IUserProfileViewModel
    {
        private string _loginName;
        private string _password;
        private string _repeatPassword;

        private static readonly UserProfileValidator ProfileValidator = new UserProfileValidator();

        public UserProfileViewModel()
        {
            
        }

        public string LoginName
        {
            get { return _loginName; }
            set
            {
                if (value == _loginName) return;
                _loginName = value;
                NotifyOfPropertyChange("LoginName");
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (value == _password) return;
                _password = value;
                NotifyOfPropertyChange("Password");
            }
        }

        public string RepeatPassword
        {
            get { return _repeatPassword; }
            set
            {
                if (value == _repeatPassword) return;
                _repeatPassword = value;
                NotifyOfPropertyChange("RepeatPassword");
            }
        }

        protected override void OnActivate()
        {
            DisplayName = "Đổi mật khẩu";
            base.OnActivate();
        }

        public void SaveCmd()
        {
            this.ValidationEnabled = true;
            if (IsValid)
            {
                var user = Globals.LoginUser;
                user.Pwd = Password;
                AsyncActions.CoroutineExecute(ProfileWorkflow.ChangePassword(user));
            }
            else
            {
                Globals.ShowMessage(" Thông tin chưa hợp lệ ");
            }
        }

        public void CloseCmd()
        {
            this.TryClose();
        }

        public override FluentValidation.Results.ValidationResult Validate()
        {
            return ProfileValidator.Validate(this);
        }

    }
}
