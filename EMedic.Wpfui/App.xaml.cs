﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace EMedic.Wpfui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private static MedicBootstrapper bootstrapper;

        public App()
        {
            bootstrapper = new MedicBootstrapper();
        }

    }
}
