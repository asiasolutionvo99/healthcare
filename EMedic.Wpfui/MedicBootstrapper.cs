﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Emc.ClientCommons.ServiceClients;
using Emc.ClientContracts.ViewModels;
using Emc.Contracts.Services;

namespace EMedic.Wpfui
{
    public class MedicBootstrapper : BootstrapperBase
    {
        private CompositionContainer container;

        public MedicBootstrapper()
        {
            Initial();
        }

        public void Initial()
        {
            
        }

        protected override void Configure()
        {
            container = new CompositionContainer(
                new AggregateCatalog(
                    AssemblySource.Instance
                        .Select(x => new AssemblyCatalog(x)).OfType<ComposablePartCatalog>()
                    ));

            var batch = new CompositionBatch();

            batch.AddExportedValue<IWindowManager>(new WindowManager());
            batch.AddExportedValue<IEventAggregator>(new EventAggregator());
            ServiceInstaller(batch);

            batch.AddExportedValue(container);

            container.Compose(batch);
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            string contract = string.IsNullOrEmpty(key) ? AttributedModelServices.GetContractName(serviceType) : key;
            var exports = container.GetExportedValues<object>(contract);

            if (exports.Any())
                return exports.First();

            throw new Exception(string.Format("Could not locate any instances of contract {0}.", contract));
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return container.GetExportedValues<object>(AttributedModelServices.GetContractName(serviceType));
        }

        private void ServiceInstaller(CompositionBatch batch)
        {
            batch.AddExportedValue(GetService<IAuthenticateService>());
        }

        private IServiceClientFactory<T> GetService<T>()
        {
            return new ServiceClientFactory<T>();
        }

        protected override void BuildUp(object instance)
        {
            container.SatisfyImportsOnce(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IShellViewModel>();
        }
    }
}
