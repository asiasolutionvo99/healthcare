﻿using System.ComponentModel.Composition;
using Caliburn.Micro;
using Emc.ClientContracts.ViewModels;

namespace EMedic.Wpfui.ViewModels
{
    [Export(typeof(IHomeViewModel))]
    public class HomeViewModel : Conductor<IScreen>.Collection.OneActive, IHomeViewModel
    {
        public HomeViewModel()
        {
            
        }

        public void ChangeViewModel(object screen)
        {
            var currentScreen = ActiveItem as IScreen;
            if (currentScreen != null)
            {
                currentScreen.TryClose();
            }

            ActiveItem = screen as IScreen;
        }
    }
}
