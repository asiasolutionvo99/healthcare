﻿using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using Emc.ClientBusinessLayers.Workflows;
using Emc.ClientCommons;
using Emc.ClientContracts.ViewModels;
using EMedic.Wpfui.Views;

namespace EMedic.Wpfui.ViewModels
{
    [Export(typeof(ILoginViewModel))]
    public class LoginViewModel : Screen, ILoginViewModel
    {
        private string _loginName;
        private PasswordBox _passwordBox;

        public LoginViewModel()
        {   
        }

        public string LoginName
        {
            get { return _loginName; }
            set
            {
                _loginName = value; 
                NotifyOfPropertyChange(() => LoginName);
            }
        }

        public void KeyUpCmd(KeyEventArgs keyEventArgs)
        {
            if (keyEventArgs.Key == Key.Enter)
            {
                DoLogin();
            }
        }

        public void LoginCmd()
        {
            DoLogin();
        }

        public void AppExitCmd()
        {
            Application.Current.Shutdown(0);
        }

        private void DoLogin()
        {
            var view = GetView() as LoginView;
            _passwordBox = view.Password;
            var password = _passwordBox.Password;
            var workflow = AuthenticateWorkflow.LoginWorkflow(LoginName, password);
            AsyncActions.CoroutineExecute(workflow);
        }


    }
}
