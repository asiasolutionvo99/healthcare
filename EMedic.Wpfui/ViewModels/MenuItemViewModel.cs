﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Caliburn.Micro;
using Emc.ClientContracts.ViewModels;

namespace EMedic.Wpfui.ViewModels
{
    public class MenuItemViewModel : PropertyChangedBase, IMenuItemViewModel
    {
        private IMenuItemViewModel _parent;
        private string _caption;
        private ObservableCollection<IMenuItemViewModel> _childs;
        private bool _isEnabled;

        public MenuItemViewModel(IMenuItemViewModel parentMenuViewModel)
        {
            Parent = parentMenuViewModel;
        }

        public MenuItemViewModel()
        {
            IsEnabled = true;
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                NotifyOfPropertyChange(() => IsEnabled);
            }
        }

        public object CommandParameter { get; set; }

        public IMenuItemViewModel Parent
        {
            get { return _parent; }
            set
            {
                _parent = value;
                NotifyOfPropertyChange(() => Parent);
            }
        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value; 
                NotifyOfPropertyChange(() => Caption);
            }
        }

        public object IconSource { get; set; }

        public ObservableCollection<IMenuItemViewModel> Childs
        {
            get { return _childs; }
            set
            {
                _childs = value; 
                NotifyOfPropertyChange(() => Childs);
            }
        }


    }
}
