﻿using System.Collections.ObjectModel;
using Emc.ClientContracts.ViewModels;

namespace EMedic.Wpfui.ViewModels
{
    public class SeparatorViewModel : IMenuItemViewModel
    {
        public SeparatorViewModel()
        {   
        }

        public SeparatorViewModel(IMenuItemViewModel parent )
        {
            Parent = parent;
        }

        public bool IsEnabled { get; set; }
        public object CommandParameter { get; set; }
        public string Command { get; set; }
        public IMenuItemViewModel Parent { get; set; }
        public ObservableCollection<IMenuItemViewModel> Childs { get; set; }
    }
}
