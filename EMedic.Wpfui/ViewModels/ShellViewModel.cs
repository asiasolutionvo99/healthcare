﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using Caliburn.Micro;
using Emc.ClientCommons;
using Emc.ClientCommons.Utilities;
using Emc.ClientContracts.ViewModels;
using Emc.ClientContracts.ViewModels.Registrations;
using Emc.ClientContracts.ViewModels.Settings;

namespace EMedic.Wpfui.ViewModels
{
    [Export(typeof(IShellViewModel))]
    public class ShellViewModel : Conductor<object>, IShellViewModel
    {
        private bool _isBusy;
        private string _userName;

        private IMenuItemViewModel _mainMenu;
        private bool _isShowMainMenu;

        public ShellViewModel()
        {
            MainMenu = new MenuItemViewModel(null);
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                NotifyOfPropertyChange(() => IsBusy);
            }
        }

        public bool IsShowMainMenu
        {
            get { return _isShowMainMenu; }
            set
            {
                _isShowMainMenu = value;
                NotifyOfPropertyChange(() => IsShowMainMenu);
            }
        }

        public bool IsLogon
        {
            get { return Globals.LoginUser != null; }
        
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                NotifyOfPropertyChange(() => UserName);
            }
        }

        public IMenuItemViewModel MainMenu
        {
            get { return _mainMenu; }
            set
            {
                _mainMenu = value; 
                NotifyOfPropertyChange(() => MainMenu);
            }
        }

        protected override void OnActivate()
        {
            DisplayName = "Medic Management System";
            base.OnActivate();
            ShowScreen<ILoginViewModel>();

        }

        public void ShowScreen<T>()
        {
            var currentScreen = ActiveItem as IScreen;
            var screen = IoC.Get<T>() as IScreen;
            if (currentScreen != null)
            {
                currentScreen.TryClose();
            }

            ActiveItem = screen;
            NotifyOfPropertyChange(() => IsLogon);
        }

        public void ShowScreen(object viewModel)
        {
            var currentScreen = ActiveItem as IScreen;
            if (currentScreen != null)
            {
                currentScreen.TryClose();
            }

            ActiveItem = viewModel;
            NotifyOfPropertyChange(() => IsLogon);
        }

        public void LogoutCmd()
        {
            Globals.LoginUser = null;
            ShowScreen<ILoginViewModel>();
        }

        public void ProfileCmd()
        {
            //AsyncActions.CoroutineActions(
            //    AsyncActions.PopupScreen<IUserProfileViewModel>(this, vm =>
            //    {
            //        vm.LoginName = UserName;
            //    })
            //);

        }

        public void HideBusyIndicator()
        {
            IsBusy = false;
        }

        public void ShowMainMenu()
        {
            IsShowMainMenu = true;

            var fileMenu = new MenuItemViewModel(MainMenu)
            {
                Caption = "Patient",
                Childs = new ObservableCollection<IMenuItemViewModel>()
            };
            fileMenu.Childs.Add(new MenuItemViewModel()
            {
                Caption = ApplicationConstant.MenuPatient,
                CommandParameter = typeof(IPatientViewModel),
                Parent = fileMenu

            });

            fileMenu.Childs.Add(new MenuItemViewModel
            {
                Caption = "Registration",
                CommandParameter = typeof(IPatientViewModel),
                Parent = fileMenu,
            });

            fileMenu.Childs.Add(new MenuItemViewModel
            {
                Caption = "Chọn thông tin",
                CommandParameter = "Select",
                Parent = fileMenu
            });

            var editMenu = new MenuItemViewModel(MainMenu)
            {
                Caption = "Level 1",
                Childs = new ObservableCollection<IMenuItemViewModel>()
            };

            var copy = new MenuItemViewModel {Caption = "Level 2", Childs = new ObservableCollection<IMenuItemViewModel>() , Parent = editMenu};
            var childCopy = new MenuItemViewModel { Caption = "Level 3", Childs = new ObservableCollection<IMenuItemViewModel>(), Parent = copy };
            var childChildCopy = new MenuItemViewModel { Caption = "Level 4", Childs = new ObservableCollection<IMenuItemViewModel>(), Parent = copy };
            
            childCopy.Childs.Add(childChildCopy);
            copy.Childs.Add( childCopy );
            editMenu.Childs.Add( copy );

            MainMenu.Childs = new ObservableCollection<IMenuItemViewModel>(
                new[]
                {
                    fileMenu, editMenu
                });

        }

        public void OnSelectMenuItem(MenuItem source)
        {
            var screenType = source.CommandParameter as Type;

            if (screenType != null)
            {
                var instance = IoC.GetInstance(screenType, null);
                ShowScreen(instance);
            }
        }

    }
}
