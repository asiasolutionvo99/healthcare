﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emc.ClientCommons;
using Emc.ClientCommons.Utilities;
using Emc.ClientContracts.Views;

namespace EMedic.Wpfui.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    [Export(typeof(ILoginView))]
    public partial class LoginView : UserControl, ILoginView
    {
        public LoginView()
        {
            InitializeComponent();
            LoadResource();
        }

        private void LoadResource()
        {
            LblLogin.Text = ApplicationConstant.LoginLabel;
            LblPassword.Text = ApplicationConstant.PasswordLabel;
            
        }
    }
}
