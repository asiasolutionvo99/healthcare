﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Emc.ClientContracts.ViewModels;

namespace EMedic.Wpfui.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
            this.Closed += ShellView_Closed;
        }

        void OnMenuItemClick(object sender, EventArgs e)
        {
            (this.DataContext as IShellViewModel).OnSelectMenuItem(sender as MenuItem);
        }

        void ShellView_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown(0);
        }
    }
}
