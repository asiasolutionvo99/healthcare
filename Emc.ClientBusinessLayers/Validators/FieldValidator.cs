﻿using Emc.ClientContracts.ViewModels.Settings;
using FluentValidation;

namespace Emc.ClientBusinessLayers.Validators
{
    public class FieldValidator : AbstractValidator<IFieldViewModel>
    {
        public FieldValidator()
        {
            RuleFor(x => x.FieldName).NotNull().NotEmpty().WithMessage(" chưa có thông tin ");
            RuleFor(x => x.DataType).NotNull().NotEmpty().WithMessage(" chưa có thông tin ");
        }
    }
}
