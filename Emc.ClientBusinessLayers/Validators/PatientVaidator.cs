﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emc.ClientContracts.ViewModels.Registrations;
using FluentValidation;

namespace Emc.ClientBusinessLayers.Validators
{
    public class PatientVaidator: AbstractValidator<IPatientViewModel>
    {
        public PatientVaidator()
        {
            RuleFor(x=> x.NationalName).NotNull().NotEmpty().WithMessage("Chưa có tên bệnh nhân");
        }
    }
}
