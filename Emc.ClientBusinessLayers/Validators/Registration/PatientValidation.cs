﻿using Emc.ClientCommons.Utilities;
using Emc.ClientContracts.ViewModels.Registrations;
using FluentValidation;

namespace Emc.ClientBusinessLayers.Validators.Registration
{
    public class PatientValidation: AbstractValidator<IPatientViewModel>
    {
        public PatientValidation()
        {
            RuleFor(x => x.Patient.PatientName).NotNull().NotEmpty().WithMessage(MessageConstant.PatientNameRequire);
            //RuleFor(x => x.Dob).NotNull().NotEmpty().WithMessage(MessageConstant.PatientDobRequire);
        }
    }
}
