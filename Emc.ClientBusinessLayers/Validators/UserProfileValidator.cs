﻿using Emc.ClientContracts.ViewModels.Settings;
using Emc.Contracts.Extensions;
using FluentValidation;
using FluentValidation.Results;

namespace Emc.ClientBusinessLayers.Validators
{
    public class UserProfileValidator : AbstractValidator<IUserProfileViewModel>
    {
        public UserProfileValidator()
        {
            RuleFor(x => x.LoginName).NotNull().NotEmpty().WithMessage(" Chưa có thông tin đăng nhập ");
            RuleFor(x => x.Password).NotNull().NotEmpty().WithMessage(" Chưa có thông tin mật khẩu ");
            Custom( IsSamePassword);
        }

        private ValidationFailure IsSamePassword(IUserProfileViewModel viewModel)
        {
            return viewModel.Password.HasContent() && viewModel.Password.Equals(viewModel.RepeatPassword)
                ? null
                : new ValidationFailure("RepeatPassword", " Mật khấu chưa khớp ");
        }
    }
}
