﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using Emc.ClientCommons;
using Emc.ClientCommons.AtomActions;
using Emc.ClientContracts.ViewModels;
using Emc.ClientContracts.ViewModels.Settings;
using Emc.Contracts.Extensions;
using Emc.Contracts.Services;
using Emc.Domains;

namespace Emc.ClientBusinessLayers.Workflows
{
    public static class AuthenticateWorkflow
    {
        public static IEnumerable<IResult> LoginWorkflow( string loginname, string password )
        {
            yield return LoaderResult.ShowBusy();
            yield return WcfServiceResult<IAuthenticateService>.CallWcf(p =>
            {
                var userDto = p.Authenticate(loginname, password);
                if (userDto.ErrorMessages.HasElements())
                {
                    Globals.ShowErrorMessages(userDto.ErrorMessages);
                }
                else if ( userDto.Entities.HasElements() )
                {
                    var user = userDto.Entities.FirstOrDefault();
                    if (user != null && user.Id > 0)
                    {
                        Globals.LoginUser = user;    
                    }
                }

            });

            if (Globals.LoginUser != null )
            {
                var homeviewModel = IoC.Get<IHomeViewModel>();
                var shellViewModel = IoC.Get<IShellViewModel>();
                var fieldViewModel = IoC.Get<IFieldViewModel>();

                shellViewModel.UserName = Globals.LoginUser.LoginName;
                shellViewModel.ShowMainMenu();
                yield return ShowScreenResult.ShowScreen(homeviewModel as IScreen);
               
            }
            else
            {
                yield return ShowMessageResult.ShowMessage(" Đăng nhập không hợp lệ ");
            }

            yield return LoaderResult.HideBusy();
        }
    }
}
