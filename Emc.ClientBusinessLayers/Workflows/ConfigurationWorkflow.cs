﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using Emc.ClientCommons;
using Emc.ClientCommons.AtomActions;
using Emc.ClientContracts.ViewModels.Registrations;
using Emc.Contracts.Extensions;
using Emc.Contracts.Services;
using Emc.Domains;

namespace Emc.ClientBusinessLayers.Workflows
{
    public static class ConfigurationWorkflow
    {

        public static List<Suburb> RefSuburbs { get; set; }

        public static IEnumerable<IResult> GetAllCountry( IPatientViewModel vm )
        {
            yield return LoaderResult.ShowBusy();
            yield return WcfServiceResult<IConfigurationService>.CallWcf(
                p =>
                {
                    var countryDto = p.GetAllCountry();
                    if (countryDto.ErrorMessages.HasElements())
                    {
                        Globals.ShowErrorMessages(countryDto.ErrorMessages);
                    }
                    else if (countryDto.Entities.HasElements())
                    {   
                        vm.Countries = countryDto.Entities.ToList();
                    }
                });

            yield return LoaderResult.HideBusy();
        }


        public static IEnumerable<IResult> GetAllprovine(IPatientViewModel vm)
        {
            yield return LoaderResult.ShowBusy();
            yield return WcfServiceResult<IConfigurationService>.CallWcf(
                p =>
                {
                    var provinDto = p.GetAllCityProvince();
                    if (provinDto.ErrorMessages.HasElements())
                    {
                        Globals.ShowErrorMessages(provinDto.ErrorMessages);
                    }
                    else if (provinDto.Entities.HasElements())
                    {
                        vm.Cityprovince  = provinDto.Entities.ToList();
                    }
                });

            yield return LoaderResult.HideBusy();
        }

        public static IEnumerable<IResult> SavePatient(Patient patient, IPatientViewModel vm)
        {
            yield return LoaderResult.ShowBusy();
            yield return WcfServiceResult<IConfigurationService>.CallWcf(
                p =>
                {
                    var patientDto = p.SavePatient(patient);
                    if (patientDto.ErrorMessages.HasElements())
                    {
                        Globals.ShowErrorMessages(patientDto.ErrorMessages);
                    }
                    else if (patientDto.Entities.HasElements())
                    {
                        vm.Patient = patientDto.Entities.First();
                    }
                });

            yield return LoaderResult.HideBusy();
        }

        //public static IEnumerable<IResult> GetAllsuburb(IPatientViewModel vm)
        //{
        //    yield return LoaderResult.ShowBusy();
        //    yield return WcfServiceResult<IConfigurationService>.CallWcf(
        //        p =>
        //        {
        //            var subDto = p.GetAllSuburb() ;
        //            if (subDto.ErrorMessages.HasElements())
        //            {
        //                Globals.ShowErrorMessages(subDto.ErrorMessages);
        //            }
        //            else if (subDto.Entities.HasElements())
        //            {
        //                vm.Suburbs  = subDto.Entities.ToList();
        //                RefSuburbs = subDto.Entities.ToList();
        //            }
        //        });

        //    yield return LoaderResult.HideBusy();
        //}
    }
}
