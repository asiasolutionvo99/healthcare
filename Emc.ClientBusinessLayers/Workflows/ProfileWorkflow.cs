﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using Emc.ClientCommons;
using Emc.ClientCommons.AtomActions;
using Emc.Contracts.DataTransferObjects;
using Emc.Contracts.Extensions;
using Emc.Contracts.Services;
using Emc.Domains;

namespace Emc.ClientBusinessLayers.Workflows
{
    public static class ProfileWorkflow
    {
        public static IEnumerable<IResult> ChangePassword(User user)
        {
            yield return LoaderResult.ShowBusy();
            yield return WcfServiceResult<IAuthenticateService>.CallWcf(proxy =>
            {
                var dto = new UserDto();
                dto.Entities.Add(user);
                dto.ActionUserId = Globals.LoginUser.Id;
                dto = proxy.SaveUser(dto);
                if (dto.ErrorMessages.HasElements())
                {
                    Globals.ShowErrorMessages(dto.ErrorMessages);
                }
                else if (dto.Entities.HasElements())
                {
                    Globals.LoginUser = dto.Entities.First();
                    Globals.ShowMessage(" Đổi mật khẩu thành công ");
                }

            });
            yield return LoaderResult.HideBusy();
        } 
    }
}
