﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using Emc.ClientCommons;
using Emc.ClientCommons.AtomActions;
using Emc.Contracts.DataTransferObjects;
using Emc.Contracts.Extensions;
using Emc.Contracts.Services;
using Emc.Domains;

namespace Emc.ClientBusinessLayers.Workflows
{
    public static class SettingWorkflow
    {
        public static IEnumerable<IResult> SaveField(Field field)
        {
            var dto = new FieldDto();
            yield return LoaderResult.ShowBusy();

            yield return WcfServiceResult<ISettingService>.CallWcf(service =>
            {
                dto.Entities.Add(field);
                dto.ActionUserId = Globals.LoginUser.Id;
                var result = service.SaveField(dto);

                if (result.ErrorMessages.HasElements())
                {
                    Globals.ShowErrorMessages(result.ErrorMessages);
                }
                else
                {
                    Globals.ShowMessage(" Lưu thành công ");
                }

            });

            yield return LoaderResult.HideBusy();
        } 
    }
}
