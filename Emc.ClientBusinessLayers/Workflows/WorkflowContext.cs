﻿using System.Collections.Generic;
using Caliburn.Micro;
using Emc.ClientCommons.Enums;

namespace Emc.ClientBusinessLayers.Workflows
{
    public class WorkflowContext
    {
        public IEnumerable<IResult> ActionResults { get; set; }
        public WorkflowResult Result { get; set; }
        public string Message { get; set; }

    }
}
