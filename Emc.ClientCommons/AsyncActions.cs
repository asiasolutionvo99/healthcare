﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Caliburn.Micro;
using Emc.ClientCommons.AtomActions;

namespace Emc.ClientCommons
{
    public static class AsyncActions
    {
        public static IEnumerable<IResult> GotoScreen<T>()
            where T : IScreen
        {
            var screen = IoC.Get<T>();
            yield return LoaderResult.ShowBusy();
            yield return ShowScreenResult.ShowScreen(screen);
            yield return LoaderResult.HideBusy();
        }

        public static IEnumerable<IResult> CallWcfService<T>( Action<T> execute )
        {
            yield return LoaderResult.ShowBusy();
            yield return WcfServiceResult<T>.CallWcf(execute);
            yield return LoaderResult.HideBusy();
        }

        public static IEnumerable<IResult> PopupScreen<T>( object parent, Action<T> initPopup = null, Action<T> afterPopup = null )
        {
            var screen = IoC.Get<T>() as IScreen;
            yield return LoaderResult.ShowBusy();
            yield return CommonResult<T>.ExcuteAction((T)screen , initPopup);
            yield return ShowScreenResult.PopupScreen(screen);
            yield return LoaderResult.HideBusy();
            yield return CommonResult<T>.ExcuteAction((T)screen, afterPopup);
        }


        public static void CoroutineExecute(IEnumerable<IResult> atomActions)
        {
            Task.Factory.StartNew(() => Coroutine.BeginExecute(atomActions.GetEnumerator()));
        }

        public static void CoroutineActions(IEnumerable<IResult> atomActions)
        {
            Coroutine.BeginExecute(atomActions.GetEnumerator());
        }
    }
}
