﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using NLog;
using LogManager = NLog.LogManager;

namespace Emc.ClientCommons.AtomActions
{
    public class CommonResult<T> : IResult
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Action<T> _action;
        private readonly T _viewModel;

        public CommonResult(T viewModel, Action<T> action)
        {
            _viewModel = viewModel;
            _action = action;
        }

        public void Execute(CoroutineExecutionContext context)
        {
            try
            {
                _action.Invoke(_viewModel);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
            }
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static CommonResult<T> ExcuteAction<T>(T viewModel, Action<T> action)
        {
            return new CommonResult<T>(viewModel, action);
        }

    }
}
