﻿using System;
using Caliburn.Micro;
using Emc.ClientContracts.ViewModels;
using NLog;
using LogManager = NLog.LogManager;

namespace Emc.ClientCommons.AtomActions
{
    public class LoaderResult : IResult
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly bool _isBusy;

        public LoaderResult(bool isBusy)
        {
            _isBusy = isBusy;
        }

        public void Execute(CoroutineExecutionContext context)
        {
            var shellViewModel = IoC.Get<IShellViewModel>();
            shellViewModel.IsBusy = _isBusy;
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static LoaderResult ShowBusy()
        {
            return new LoaderResult(true);

        }

        public static LoaderResult HideBusy()
        {
            return new LoaderResult(false);
        }

    }
}
