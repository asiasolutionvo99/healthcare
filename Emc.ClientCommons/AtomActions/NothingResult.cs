﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Caliburn.Micro;

namespace Emc.ClientCommons.AtomActions
{
    public class NothingResult : IResult
    {
        public void Execute(CoroutineExecutionContext context)
        {
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate {};
    }
}
