﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Windows;
using Caliburn.Micro;
using Action = System.Action;

namespace Emc.ClientCommons.AtomActions
{
    public class PopupResult : IResult
    {
        private readonly dynamic _settings;
        private readonly object _viewmodel;
        private readonly object _parentVm;
        private readonly Action _action;

        public PopupResult(object viewmodel, dynamic settings)
        {
            _viewmodel = viewmodel;
            _settings = settings;
        }

        public PopupResult(object viewmodel, Action action, dynamic settings)
        {
            _viewmodel = viewmodel;
            _settings = settings;
            _action = action;
        }

        public PopupResult(object viewmodel, object parentVm, Action action, dynamic settings)
        {
            _viewmodel = viewmodel;
            _settings = settings;
            _action = action;
            _parentVm = parentVm;
        }

        public object ViewModel { get { return _viewmodel; } }
        public object ParentViewModel { get { return _parentVm; } }

        public void Execute(CoroutineExecutionContext context)
        {
            var windowManager = IoC.Get<IWindowManager>();
            windowManager.ShowWindow(_viewmodel, null, _settings);
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static PopupResult Show<T>()
        {
            var viewModel = IoC.Get<T>();
            return new PopupResult(viewModel, BuildSettings());
        }

        public static PopupResult Show<T>(object parent)
        {
            var viewModel = IoC.Get<T>();
            return new PopupResult(viewModel, BuildSettings());
        }

        public static PopupResult ShowSmall<T>(Action action)
        {
            dynamic settings = BuildSettings();
            settings.Width = 300;
            settings.Height = 200;
            var viewModel = IoC.Get<T>();
            return new PopupResult(viewModel, action, settings);
        }

        public static PopupResult ShowMedium<T>(Action action)
        {
            dynamic settings = BuildSettings();
            settings.Width = 400;
            settings.Height = 200;
            var viewModel = IoC.Get<T>();
            return new PopupResult(viewModel, action, settings);
        }

        public static PopupResult ShowLarge<T>(Action action)
        {
            dynamic settings = BuildSettings();
            settings.Width = 600;
            settings.Height = 200;
            var viewModel = IoC.Get<T>();
            return new PopupResult(viewModel, action, settings);
        }

        public static dynamic BuildSettings()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            settings.SizeToContent = SizeToContent.Manual;
            settings.ShowInTaskbar = false;
            settings.Title = "Display";
            return settings;
        }

    }
}
