﻿using System;
using System.Windows;
using Caliburn.Micro;

namespace Emc.ClientCommons.AtomActions
{
    public class ShowMessageResult : IResult
    {
        private readonly string _message;

        public ShowMessageResult( string message )
        {
            _message = message;
        }

        public void Execute(CoroutineExecutionContext context)
        {
            MessageBox.Show(_message);
            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate {};

        public static IResult ShowMessage(string message)
        {
            return new ShowMessageResult(message);
        }
        
    }
}
