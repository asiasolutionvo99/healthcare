﻿using System;
using Caliburn.Micro;
using Emc.ClientContracts.ViewModels;
using NLog;
using LogManager = NLog.LogManager;

namespace Emc.ClientCommons.AtomActions
{
    public class ShowScreenResult : IResult
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IScreen _viewModel;
        private readonly bool _isPopup;

        public ShowScreenResult(IScreen viewModel)
        {
            _viewModel = viewModel;
        }

        public ShowScreenResult(bool isPopup, IScreen viewModel)
        {
            _viewModel = viewModel;
            _isPopup = isPopup;
        }

        public void Execute(CoroutineExecutionContext context)
        {
            try
            {   
                if (!_isPopup)
                {
                    var shellVm = IoC.Get<IShellViewModel>();
                    shellVm.ShowScreen(_viewModel);
                }
                else
                {
                    Caliburn.Micro.Execute.OnUIThread(() =>
                    {
                        var windowManager = IoC.Get<IWindowManager>();
                        windowManager.ShowWindow(_viewModel);
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorException(ex.Message, ex);
            }

            Completed(this, new ResultCompletionEventArgs());
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static ShowScreenResult ShowScreen<T>(T viewmodel)
            where T : IScreen
        {
            return new ShowScreenResult(viewmodel);
        }

        public static IResult PopupScreen<T>(T viewmodel)
        {
            return new ShowScreenResult(true, viewmodel as IScreen);
        }

    }
}
