﻿using System;
using System.Threading;
using Caliburn.Micro;
using Emc.ClientCommons.ServiceClients;
using NLog;
using LogManager = NLog.LogManager;

namespace Emc.ClientCommons.AtomActions
{

    public class WcfServiceResult<T> : IResult
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Action<T> _executeAction;

        public WcfServiceResult(Action<T> executeAction)
        {
            _executeAction = executeAction;
        }

        public void Execute(CoroutineExecutionContext context)
        {
            var resultEvent = new ResultCompletionEventArgs();
            try
            {
                Thread.Sleep(100);
                var serviceClientFactory = IoC.Get<IServiceClientFactory<T>>();
                T proxy = serviceClientFactory.GetServiceClient();
                _executeAction.Invoke(proxy);
            }
            catch (SystemException ex)
            {
                Logger.ErrorException(ex.Message, ex);
                resultEvent.Error = ex;
            }

            Completed(this, resultEvent);
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

        public static WcfServiceResult<T> CallWcf(Action<T> executeAction)
        {
            return new WcfServiceResult<T>(executeAction);
        } 

    }
}
