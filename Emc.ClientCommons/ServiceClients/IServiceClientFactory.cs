﻿using System;

namespace Emc.ClientCommons.ServiceClients
{
    public interface IServiceClientFactory<out T> : IDisposable
    {
        /// <summary>
        /// Gets the service client.
        /// </summary>
        /// <returns>The service proxy.</returns>
        T GetServiceClient();
    }
}
