﻿using System;
using System.ServiceModel;

namespace Emc.ClientCommons.ServiceClients
{
    public class ServiceClientFactory<T> : IServiceClientFactory<T>
    {
        private readonly string _configurationName;
        private T _clientProxy;

        public ServiceClientFactory()
        {
            _configurationName = typeof(T).FullName.Replace("Async", "");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceClientFactory{T}"/> class.
        /// </summary>
        /// <param name="configurationName">Name of the configuration.</param>
        public ServiceClientFactory(string configurationName)
        {
            _configurationName = configurationName;
        }

        /// <summary>
        /// Gets the service client.
        /// </summary>
        /// <returns></returns>
        public T GetServiceClient()
        {
            return this.GetServiceProxy();
        }

        /// <summary>
        /// Gets the service.
        /// </summary>
        /// <returns></returns>
        private T GetServiceProxy()
        {
            var channelFactory = new ChannelFactory<T>(_configurationName);
            _clientProxy = channelFactory.CreateChannel();
            ((IClientChannel)_clientProxy).Open();
            return _clientProxy;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="isDisposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                var proxy = _clientProxy as IDisposable;
                if (proxy != null)
                {
                    proxy.Dispose();
                }
            }
        }
    }
}
