﻿namespace Emc.ClientCommons.Utilities
{
    public static class ApplicationConstant
    {
        public const string Language = "vi";
        public const string MessageFileSetting = "MessageResources";
        public const string ExitQuestion = "Exit.Question";
        public const string AppTitle = "";
        public const string LoginGroupLabel = "LoginGroup.Label";
        public const string LoginLabel = "Login.Label";
        public const string PasswordLabel = "Password.Label";



        #region Menu
        public const string MenuPatient = "Patient";

        #endregion
    }
}
