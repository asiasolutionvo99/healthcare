﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Emc.ClientCommons.Utilities
{
    public static class MessageHelpers
    {
        private readonly static IDictionary<string, string> Messages = new Dictionary<string, string>();

        private const string LangTagName = "lang";
        private const string MessagesTagName = "messages";
        private const string MessageTagName = "message";
        private const string KeyAttributeName = "key";
        private const string ValueAttributeName = "value";

        public static void Initial()
        {
            var resourceFile = System.Configuration.ConfigurationManager
                .AppSettings[ApplicationConstant.MessageFileSetting];
            
            var xElements = XElement.Load(resourceFile);
            var elements = xElements.Elements(MessagesTagName);

            foreach (var xElement in elements)
            {
                if ( string.Equals(ApplicationConstant.Language, 
                    xElement.Attribute(LangTagName).Value, 
                    StringComparison.OrdinalIgnoreCase))
                {
                    var messageElements = xElement.Elements(MessageTagName);
                    foreach (var messageElement in messageElements)
                    {
                        var key = messageElement.Attribute(KeyAttributeName).Value;
                        var messageValue = messageElement.Attribute(ValueAttributeName).Value;
                        Messages[key] = messageValue;
                    }
                }
            }
        }

        public static string GetMessage(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return string.Empty;
            }

            if ( Messages.ContainsKey(key))
            {
                return Messages[key];
            }

            return key;
        }

    }
}
