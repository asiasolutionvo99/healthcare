﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Caliburn.Micro;
using FluentValidation.Results;

namespace Emc.ClientCommons.ViewModels
{

    public abstract class BaseFormViewModel : Screen, IDataErrorInfo
    {

        protected BaseFormViewModel()
        {
            
        }

        public bool ValidationEnabled { get; set; }

        public string Error
        {
            get { return GetError(Validate()); }
        }

        public string this[string columnName]
        {
            get
            {
                if (!ValidationEnabled)
                    return null;

                var validationResults = Validate();
                if (validationResults == null) return string.Empty;
                var columnResults =
                    validationResults.Errors.FirstOrDefault<ValidationFailure>(
                    x => string.Equals(x.PropertyName, columnName));
                return columnResults != null ? columnResults.ErrorMessage : string.Empty;
            }
        }

        public bool IsValid
        {
            get { return Validate().IsValid; }
        }

        public abstract ValidationResult Validate();

        public static string GetError(ValidationResult result)
        {
            var validationErrors = new StringBuilder();
            foreach (var validationFailure in result.Errors)
            {
                validationErrors.Append(validationFailure.ErrorMessage);
                validationErrors.Append(Environment.NewLine);
            }

            return validationErrors.ToString();
        }

    }
}
