﻿using System.Collections.ObjectModel;

namespace Emc.ClientContracts.ViewModels
{
    public interface IMenuItemViewModel
    {
        bool IsEnabled { get; set; }
        object CommandParameter { get; set; }
        IMenuItemViewModel Parent { get; set; }
        ObservableCollection<IMenuItemViewModel> Childs { get; set; } 
    }
}
