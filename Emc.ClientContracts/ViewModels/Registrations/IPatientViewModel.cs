﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emc.Domains;

namespace Emc.ClientContracts.ViewModels.Registrations
{
    public interface IPatientViewModel
    {
        string PatientName { get; set; }
        string Dob { get; set; }
        List<Country> Countries { get; set; }
        List<CityProvince> Cityprovince { get; set; }
        List<Suburb> Suburbs { get; set; }
        Patient Patient { get; set; }
   }
}
