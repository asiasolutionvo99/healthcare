﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emc.ClientContracts.ViewModels.Settings
{
    public interface IFieldViewModel
    {
        string FieldName { get; set; }
        string DataType { get; set; }
    }
}
