﻿namespace Emc.ClientContracts.ViewModels.Settings
{
    public interface IUserProfileViewModel
    {

        string LoginName { get; set; }
        string Password { get; set; }
        string RepeatPassword { get; set; }

    }
}
