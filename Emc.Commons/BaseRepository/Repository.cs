﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Emc.Commons.Contracts;
using Emc.Commons.Contracts.Utilities;
using Emc.Commons.Utilities;
using Emc.Domains.Contracts;
using NLog;

namespace Emc.Commons.BaseRepository
{
    public class Repository<T> : IRepository<T> 
        where T : class, IAggregatorRoot
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private const int CommandTimeout = 600;
        private readonly IDataContext _context;
        private readonly IDbInfo<T> _dbInfo; 

        public Repository(IDataContext context)
        {
            _context = context;
            _dbInfo = _context.Db<T>();
        }

        public void Dispose()
        {   
            GC.SuppressFinalize(this);
        }

        public long? Add(T item)
        {
            var id = _context.Connection.Query<long>(_dbInfo.InsertQuery(), item, 
                _context.CurrenTransaction
                , commandTimeout: CommandTimeout)
                .First();

            item.Id = id;
            return id;
        }

        public int Remove(T item)
        {
            return _context.Connection.Execute(_dbInfo.DeleteQuery(), item.Id, _context.CurrenTransaction);
        }

        public int Update(T item)
        {
            return _context.Connection.Execute(_dbInfo.UpdateQuery(), item, _context.CurrenTransaction);
        }

        public T FindById(long id)
        {
            var dbInfo = _context.Db<T>();
            var query = Query.WithBuilder()
                .Select<T>()
                .From(dbInfo.TableName)
                .BeginWhere()
                .And(dbInfo.PrimaryKey, Operator.Equals, "@Id")
                .EndWhere();

            return _context.Connection.Query<T>(query.ToSqlQuery(), new { Id = id }
                , _context.CurrenTransaction
                , commandTimeout: CommandTimeout)
                .FirstOrDefault();
        }

        public T Find(IQuery query)
        {
            return _context.Connection.Query<T>(query.ToSqlQuery()).FirstOrDefault();
        }

        public IEnumerable<T> Filter(IQuery query)
        {
            return _context.Connection.Query<T>(query.ToSqlQuery());
        }

        public IEnumerable<T> FindAll()
        {
            var dbInfo = _context.Db<T>();
            var query = Query.WithBuilder().Select<T>().From(dbInfo.TableName);
            return _context.Connection.Query<T>(query.ToSqlQuery());
        }
    }
}
