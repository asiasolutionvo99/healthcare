﻿using System;
using System.Collections;
using System.Data;
using Emc.Commons.Contracts;
using Emc.Domains.Contracts;

namespace Emc.Commons.BaseRepository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Hashtable _repositories = new Hashtable();
        private readonly IDataContext _context;
        private readonly IDbConnection _connection;
        private IDbTransaction _transaction;
        
        public UnitOfWork(IDataContext context)
        {
            _context = context;
            _connection = _context.Connection;
            _connection.Open();
        }

        public void BeginTransaction()
        {   
            _transaction = _connection.BeginTransaction();
        }

        public IRepository<T> Repository<T>() where T : class, IAggregatorRoot
        {
            var name = typeof (T).Name;

            if (!_repositories.ContainsKey(name))
            {
                _repositories[name] = new Repository<T>(_context);
            }

            return _repositories[name] as IRepository<T>;
        }

        public void Commit()
        {
            if (_transaction != null)
            {
                _transaction.Commit();
            }
        }

        public void Rollback()
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
            }
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
            }

            if (_connection != null)
            {   
                _connection.Dispose();
            }
            GC.SuppressFinalize(this);
        }

    }
}
