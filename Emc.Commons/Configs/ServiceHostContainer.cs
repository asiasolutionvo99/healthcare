﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using Castle.Facilities.WcfIntegration;
using Castle.Windsor;

namespace Emc.Commons.Configs
{
    public class ServiceHostContainer : IDisposable
    {

        private List<ServiceHostBase> _hosts = new List<ServiceHostBase>();
        private readonly IWindsorContainer _container;

        public ServiceHostContainer(IWindsorContainer container)
        {
            _container = container;
        }

        /// <summary>
        /// Initialize the service container and spawn all configured services
        /// </summary>
        public void Initialize()
        {
            Configuration appConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ServiceModelSectionGroup serviceModel = ServiceModelSectionGroup.GetSectionGroup(appConfig);

            if (appConfig == null || serviceModel == null)
            {
                throw new ConfigurationErrorsException("Configuration section 'system.serviceModel' not found");
            }

            ServicesSection services = serviceModel.Services;

            var serviceHostFactory = new DefaultServiceHostFactory(_container.Kernel);
            InitializeServices(serviceHostFactory, services.Services);
        }

        private void InitializeServices(DefaultServiceHostFactory serviceHostFactory, ServiceElementCollection services)
        {
            foreach (ServiceElement element in services)
            {
                ServiceHostBase serviceHost = serviceHostFactory.CreateServiceHost(element.Name, new Uri[0]);
                serviceHost.Faulted += ServiceHost_Faulted;
                serviceHost.Open();

                var serviceDescription = serviceHost.Description;
                Console.WriteLine("---------------------------------------------------------------------");
                Console.WriteLine(serviceDescription.ConfigurationName);

                foreach (var serviceEndpoint in serviceDescription.Endpoints)
                {
                    Console.WriteLine("Address: " + serviceEndpoint.Address);
                    Console.WriteLine("Binding Name: " + serviceEndpoint.Binding.Name);
                    Console.WriteLine("Contract Name: " + serviceEndpoint.Contract.Name);
                }

                Console.WriteLine("Configuration Name: " + serviceDescription.ConfigurationName);
                Console.WriteLine("---------------------------------------------------------------------");
                Console.WriteLine();

                _hosts.Add(serviceHost);
            }
        }

        public void Close()
        {
            // Iterate through all the hosted services and close them
            foreach (ServiceHostBase host in _hosts)
            {
                host.Close();
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDispose)
        {
            if (isDispose)
            {   
                if (_hosts != null)
                {
                    foreach (var serviceHostBase in _hosts)
                    {
                        serviceHostBase.Close();
                    }

                    _hosts.Clear();
                }

                _hosts = null;
            }
        }

        private void ServiceHost_Faulted(object sender, EventArgs e)
        {
            ServiceHostBase host = sender as ServiceHostBase;

            if (host != null)
            {
                host.Abort();
            }

            // remove existing service from the list
            if (_hosts.Contains(host))
            {
                _hosts.Remove(host);
            }
        }
    }
}
