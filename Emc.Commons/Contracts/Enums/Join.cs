﻿namespace Emc.Commons.Contracts.Enums
{
    public enum Join
    {
        InnerJoin,
        LeftJoin,
        RightJoin,

    }
}
