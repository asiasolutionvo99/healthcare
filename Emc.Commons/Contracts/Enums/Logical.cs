﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emc.Commons.Contracts.Enums
{
    public enum Logical
    {
        And,
        Or
    }
}
