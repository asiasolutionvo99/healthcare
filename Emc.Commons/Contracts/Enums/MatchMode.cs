﻿namespace Emc.Commons.Contracts.Enums
{
    public enum MatchMode
    {
        Anywhere,
        StartWiths,
        EndWiths,
        Exact,
    }
}
