﻿namespace Emc.Commons.Contracts.Enums
{
    public enum Operator
    {
        Equals,
        LessThan,
        LessThanOrEqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
        Like,
        In,
        Between,
    }
}
