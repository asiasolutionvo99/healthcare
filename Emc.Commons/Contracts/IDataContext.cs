﻿using System;
using System.Data;
using Emc.Domains.Contracts;

namespace Emc.Commons.Contracts
{
    public interface IDataContext : IDisposable
    {
        IDbConnection Connection { get; }
        IDbInfo<T> Db<T>() where T : class, IAggregatorRoot;
        IDbTransaction CurrenTransaction { get; set; }

    }
}
