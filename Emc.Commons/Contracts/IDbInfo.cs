﻿using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Contracts
{
    public interface IDbInfo<T>
    {
        string TableName { get; }
        string PrimaryKey { get; }

        string InsertQuery();
        string UpdateQuery();
        string DeleteQuery();

    }
}
