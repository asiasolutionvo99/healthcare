﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Emc.Commons.Contracts
{
    public interface IMapping<T>
    {
        IMapping<T> Table(string tableName);
        IMapping<T> WithId(string idColumn);

    }
}
