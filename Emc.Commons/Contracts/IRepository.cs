﻿using System;
using System.Collections.Generic;
using Emc.Commons.Contracts.Utilities;
using Emc.Domains.Contracts;

namespace Emc.Commons.Contracts
{
    public interface IRepository<T> : IDisposable where T : class, IAggregatorRoot
    {
        long? Add(T item);
        int Remove(T item);
        int Update(T item);
        T FindById(long id);

        T Find(IQuery query);
        IEnumerable<T> Filter(IQuery query);
        IEnumerable<T> FindAll();

    }
}
