﻿namespace Emc.Commons.Contracts
{
    public interface ITransform<in TInput, out TResult>
    {
        TResult Transform(TInput input);
    }
}
