﻿using System.Xml.Linq;

namespace Emc.Commons.Contracts
{
    public interface ITransformToXml<in T> : ITransform<T, XElement>
    {
    }
}
