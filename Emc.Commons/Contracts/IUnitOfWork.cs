﻿using System;
using Emc.Domains.Contracts;

namespace Emc.Commons.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();
        IRepository<T> Repository<T>() where T : class, IAggregatorRoot;
        void Commit();
        void Rollback();
    }
}
