﻿namespace Emc.Commons.Contracts.Utilities
{
    public interface IConditionClause
    {
        Conjunction Conjunction { get; set; }
        string Condition { get; set; }
        
    }

    public enum Conjunction
    {
        And,
        Or
    }

}
