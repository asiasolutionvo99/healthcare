﻿using System;
using System.Linq.Expressions;

namespace Emc.Commons.Contracts.Utilities
{
    public interface IInsertQuery
    {
        IInsertQuery Fields<T>(params Expression<Func<T, object>>[] fields);
        IInsertQuery IgnoreField<T>(params Expression<Func<T, object>>[] fields);
        IInsertQuery IgnoreField(params string[] fields);

        string ToSqlInsert();
    }
}
