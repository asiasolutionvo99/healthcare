﻿using System;
using System.Linq.Expressions;

namespace Emc.Commons.Contracts.Utilities
{
    public interface IJoinLinq<T>
    {
        IJoinLinq<T> On();
        IQuery Compare<TRight>(Expression<Func<T, object>> left, Operator op, Expression<Func<T, object>> right);
        string ToJoinQuery();
    }
}
