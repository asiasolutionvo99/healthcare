﻿using System;
using System.Data;
using System.Linq.Expressions;

namespace Emc.Commons.Contracts.Utilities
{
    public interface IQuery
    {
        CommandType CommandType { get; set; }
        IQuery Select(params string[] columns);
        IQuery Select<T>(params Expression<Func<T, object>>[] columns);

        IQuery From(string tableParam);
        IQuery From<T>(string tableParam);
        IQuery From(string tableParam, string alias);

        IQueryJoin Join();
        IQueryWhere BeginWhere();
        IQuery GroupBy(params string[] groupByParams);
        IQuery OrderBy(params string[] orderByParams);

        string ToSqlQuery();

        void Add(IQueryJoin joinClause);
        void Add<T>(IJoinLinq<T> joinClause);

        void Add(IQueryWhere whereClause);
        void Add(params IConditionClause[] conditions);

    }

    public enum Operator
    {
        Equals,
        LessThan,
        LessThanOrEqualTo,
        GreaterThan,
        GreaterThanOrEqualTo,
        Like,
        In,
        Between,
    }

    public enum Join
    {
        InnerJoin,
        LeftJoin,
        RightJoin,
   
    }

    public enum MatchMode
    {
        Anywhere,
        StartWiths,
        EndWiths,
        Exact,
    }

}
