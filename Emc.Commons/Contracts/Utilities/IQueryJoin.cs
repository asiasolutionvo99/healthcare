﻿namespace Emc.Commons.Contracts.Utilities
{
    public interface IQueryJoin
    {
        IQueryJoin Table(Join joinOp, string tablename);
        IJoinLinq<T> Table<T>(Join joinOp, string tablename);
        IQueryJoin On();
        IQuery Compare(string leftProperty, Operator op, string rightProperty);

        string ToJoinQuery();
    }
}
