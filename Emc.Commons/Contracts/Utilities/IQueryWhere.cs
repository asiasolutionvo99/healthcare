﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Emc.Commons.Contracts.Utilities
{
    public interface IQueryWhere
    {
        IQueryWhere AndIf(bool hasCondition, string column, Operator op, string value);
        IQueryWhere And(string column, Operator op, string value);
        IQueryWhere AndIf<T>(bool hasCondition, Expression<Func<T, object>> field, Operator op, params string[] value);
        IQueryWhere And<T>(Expression<Func<T, object>> field, Operator op, params string[] value);
        IQueryWhere Or(string column, Operator op, string value);
        IQuery EndWhere();
        IUpdateQuery EndWhereUpdate();
        IList<IConditionClause> Conditions();

    }
}
