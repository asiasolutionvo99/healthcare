﻿using System;
using System.Linq.Expressions;

namespace Emc.Commons.Contracts.Utilities
{
    public interface IUpdateQuery
    {
        
        IUpdateQuery Set<T>(Expression<Func<T, object>> field);
        IUpdateQuery AndSet<T>(Expression<Func<T, object>> field);
        IUpdateQuery SetAll<T>();
        void Ignore(string fieldName);
        void Ignore<T>(Expression<Func<T, object>> field);
        IQueryWhere Where();
        void Add(IQueryWhere queryWhere);
        string ToSqlUpdate();
    }
}
