﻿using System;
using System.Linq.Expressions;
using System.Text;

namespace Emc.Commons.Contracts.Utilities
{
    public static class LinqHelper
    {
        public static string ToSqlFieldName<T>(this Expression<Func<T, object>> action)
        {
            var propname = "";
            var prefix = typeof (T).Name;
            if (action.Body is MemberExpression)
            {
                var expression = (MemberExpression)action.Body;
                propname = expression.Member.Name;
                return string.Format("[{0}].[{1}] ", prefix, propname);
            }
            else if (action.Body is UnaryExpression)
            {
                var expression = (MemberExpression)((UnaryExpression)action.Body).Operand;
                propname = expression.Member.Name;
                return string.Format("[{0}].[{1}] ", prefix, propname);
            }
            
            throw new NotSupportedException();
             
        }

        public static string ToSqlParameter<T>(this Expression<Func<T, object>> action)
        {
            var propname = "";
            if (action.Body is MemberExpression)
            {
                var expression = (MemberExpression)action.Body;
                propname = expression.Member.Name;
                return string.Format("{0} ", propname);
            }
            
            if (action.Body is UnaryExpression)
            {
                var expression = (MemberExpression)((UnaryExpression)action.Body).Operand;
                propname = expression.Member.Name;
                return string.Format("{0}", propname);
            }

            throw new NotSupportedException();

        }

        public static string ToAliasName<T>(this Expression<Func<T, object>> action)
        {
            var expression = (MemberExpression)action.Body;
            return expression.Member.Name;
        }

        public static string ToSqlConditions(this IConditionClause[] conditions, bool isBuildWhere = true)
        {
            var sqlBuilder = new StringBuilder();
            if ( conditions != null && conditions.Length > 0)
            {
                for (int i = 0; i < conditions.Length; i++)
                {
                    var condition = conditions[i];
                    if (i == 0 && isBuildWhere )
                    {
                        sqlBuilder.AppendFormat(" WHERE {0} ", condition.Condition);
                    }
                    else if (condition.Conjunction == Conjunction.And)
                    {
                        sqlBuilder.AppendFormat(" AND ({0}) ", condition.Condition);
                    }
                    else if (condition.Conjunction == Conjunction.Or)
                    {
                        sqlBuilder.AppendFormat(" OR ({0}) ", condition.Condition);
                    }
                }

                sqlBuilder.Append("\r\n");
            }

            return sqlBuilder.ToString();
        } 

        public static string ToJoinOperator(this Join joinOp)
        {
            switch (joinOp)
            {
                case Join.LeftJoin:
                    return " LEFT JOIN ";

                case Join.InnerJoin:
                    return " INNER JOIN ";

                case Join.RightJoin:
                    return " RIGHT JOIN ";

                default:
                    return "";
            }
        }

        public static string ToConditionOperator(this Operator op)
        {
            switch (op)
            {
                case Operator.Between:
                    return " BETWEEN ";

                case Operator.Equals:
                    return " = ";

                case Operator.LessThan:
                    return " < ";

                case Operator.LessThanOrEqualTo:
                    return " <= ";

                case Operator.GreaterThan:
                    return " > ";

                case Operator.GreaterThanOrEqualTo:
                    return " >= ";

                case Operator.In:
                    return " IN ";
                case Operator.Like:
                    return " LIKE ";

                default:
                    throw new NotSupportedException();
                    
            }
        }

    }
}
