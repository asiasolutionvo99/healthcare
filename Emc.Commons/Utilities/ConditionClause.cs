﻿using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class ConditionClause : IConditionClause
    {
        public ConditionClause()
        {
            
        }

        public ConditionClause( Conjunction conjunction, string condition )
        {
            Conjunction = conjunction;
            Condition = condition;
        }

        public Conjunction Conjunction { get; set; }
        public string Condition { get; set; }
    }
}
