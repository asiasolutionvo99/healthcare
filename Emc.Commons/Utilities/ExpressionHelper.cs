﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Emc.Commons.Utilities
{
    public static class QueryHelpers
    {
        public static Expression<Func<T, bool>> AndAlso<T>(
            this Expression<Func<T, bool>> left,
            Expression<Func<T, bool>> right)
        {
            return BinaryOnExpressions(left, ExpressionType.AndAlso, right);
        }

        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        {
            return BinaryOnExpressions(left, ExpressionType.OrElse, right);
        }

        public static Expression<Func<T, bool>> BinaryOnExpressions<T>(
                            this Expression<Func<T, bool>> left,
                            ExpressionType binaryType,
                            Expression<Func<T, bool>> right)
        {
            
            var map = left.Parameters.Select((f, i) => new { f, s = right.Parameters[i] })
                .ToDictionary(p => p.s, p => p.f);
            
            var rightBody = ParameterRebinder.ReplaceParameters(map, right.Body);
            
            var binExpression = Expression.MakeBinary(binaryType, left.Body, rightBody);
            return Expression.Lambda<Func<T, bool>>(binExpression, left.Parameters);
        }

    }

    internal class ParameterRebinder : ExpressionVisitor
    {
        private readonly Dictionary<ParameterExpression, ParameterExpression> map;

        public ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
        {
            this.map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
        }

        public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
        {
            return new ParameterRebinder(map).Visit(exp);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            ParameterExpression replacement;
            if (map.TryGetValue(p, out replacement))
            {
                p = replacement;
            }
            return base.VisitParameter(p);
        }
    }

}
