﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class InsertIntoQuery : IInsertQuery
    {
        private readonly StringBuilder _sqlBuilder = new StringBuilder(100);

        private readonly string _tableName;
        private readonly List<string> _fieldParameters = new List<string>();
        private readonly List<string> _ignores = new List<string>(); 

        public InsertIntoQuery( string tableName)
        {
            _tableName = tableName;
        }

        public IInsertQuery Fields<T>(params Expression<Func<T, object>>[] fields)
        {
            if (fields != null && fields.Length > 0)
            {
                _fieldParameters.AddRange(fields.Select(x => x.ToSqlParameter()));
            }
            else
            {
                var type = typeof (T);
                var properties = type.GetProperties();

                foreach (var propertyInfo in properties)
                {
                    _fieldParameters.Add(propertyInfo.Name);
                    
                }

            }

            return this;
        }

        public IInsertQuery IgnoreField<T>(params Expression<Func<T, object>>[] fields)
        {
            _ignores.AddRange(fields.Select(x => x.ToSqlParameter().ToLower()));
            return this;
        }

        public IInsertQuery IgnoreField(params string[] fields)
        {
            _ignores.AddRange(fields.Select(x => x.ToLower()));
            return this;
        }

        public string ToSqlInsert()
        {
            var fieldBuilder = new StringBuilder();
            var parameterBuilder = new StringBuilder();
            var isFirst = true;
            foreach (string fieldname in _fieldParameters)
            {
                if (_ignores.Contains(fieldname.ToLower()))
                {
                    continue;
                }

                if (isFirst)
                {
                    isFirst = false;
                    fieldBuilder.Append(fieldname);
                    parameterBuilder.Append("@").Append(fieldname);
                }
                else
                {
                    fieldBuilder.Append(",").Append(fieldname);
                    parameterBuilder.Append(", @").Append(fieldname);
                }
            }

            _sqlBuilder.AppendFormat(" INSERT INTO {0}({1}) VALUES ({2}); SELECT CAST(scope_identity() AS BIGINT) AS ID; ", _tableName, fieldBuilder, parameterBuilder);

            return _sqlBuilder.ToString();
        }
    }
}
