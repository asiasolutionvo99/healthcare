﻿using System;
using System.Linq.Expressions;
using System.Text;
using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class JoinLinq<T> : IJoinLinq<T>
    {
        private readonly StringBuilder _joinBuilder = new StringBuilder(50);
        private readonly IQuery _query;
        
        public JoinLinq( string joinOp, string tableName, IQuery query )
        {
            _query = query;
            _joinBuilder.AppendFormat(" {0} {1} [{2}] ", joinOp, tableName , typeof (T).Name);
        }

        public IJoinLinq<T> On()
        {
            _joinBuilder.Append(" ON ");
            return this;
        }

        public IQuery Compare<TRight>(Expression<Func<T, object>> left, Operator op, Expression<Func<T, object>> right)
        {
            _joinBuilder.AppendFormat(" {0} = {1} ", left.ToSqlFieldName(), right.ToSqlFieldName());
            
            _query.Add(this);
            return _query;
        }

        public string ToJoinQuery()
        {
            return _joinBuilder.ToString();
        }
    }
}
