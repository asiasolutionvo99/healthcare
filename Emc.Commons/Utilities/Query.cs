﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class Query : IQuery
    {
        private readonly StringBuilder _sqlBuilder = new StringBuilder(500);
        private readonly List<string> _selectParams = new List<string>();
        private readonly IList<string> _tableParams = new List<string>();
        private readonly List<string> _groupByParams = new List<string>();
        private readonly List<string> _orderByParams = new List<string>();
        private readonly IList<string> _joinClause = new List<string>();
        private readonly List<IQueryWhere> _filterParams = new List<IQueryWhere>();
        private readonly List<IConditionClause> _conditions = new List<IConditionClause>();

        public Query(CommandType commandType)
        {
            CommandType = commandType;
        }

        public static IQuery WithBuilder()
        {
            return new Query(CommandType.Text);
        }

        public static IQuery WithSp(string spName)
        {
            return new Query(CommandType.StoredProcedure);
        }

        public static IInsertQuery InsertInto(string tableName)
        {
            return new InsertIntoQuery(tableName);
        }

        public static IUpdateQuery Update(string tableName)
        {
            return new UpdateQuery(tableName);
        }

        public CommandType CommandType { get; set; }

        public IQuery Select(params string[] columns)
        {
            _selectParams.AddRange(columns);
            return this;
        }

        public IQuery Select<T>(params Expression<Func<T, object>>[] columns)
        {
            if (columns == null || columns.Length == 0)
            {
                _selectParams.Add(string.Format(" {0}.* ", typeof(T).Name));
            }
            else
            {
                _selectParams.AddRange(columns.Select(x => x.ToSqlFieldName()));
            }

            return this;
        }

        public IQuery From(string tableName)
        {
            _tableParams.Add(tableName);
            return this;
        }

        public IQuery From<T>(string tableParam)
        {
            var tableName = string.Format(" {0} [{1}]", tableParam, typeof (T).Name);
            _tableParams.Add(tableName);
            return this;
        }

        public IQuery From(string tableParam, string alias)
        {
            var tableName = string.Format(" {0} {1}  ", tableParam, alias);
            From(tableName);
            return this;
        }

        public IQueryJoin Join<T>()
        {
            return new QueryJoin(this);
        }

        public IQueryJoin Join()
        {
            return new QueryJoin(this);
        }

        public IQueryWhere BeginWhere()
        {
            return new QueryWhere(this); 
        }

        public IQuery GroupBy(params string[] groupByParams)
        {
            _groupByParams.AddRange(groupByParams);
            return this;
        }

        public IQuery OrderBy(params string[] orderByParams)
        {
            _orderByParams.AddRange(orderByParams);
            return this;
        }

        public IInsertQuery InsertInto<T>(string tableName)
        {
            return new InsertIntoQuery(tableName);
        }

        private void BuildSelect()
        {
            if (_selectParams.Count > 0)
            {
                _sqlBuilder.AppendFormat(" SELECT {0} ", string.Join(",", _selectParams));
            }
            else
            {
                _sqlBuilder.Append(" SELECT * ");
            }

            _sqlBuilder.Append("\r\n")
                       .AppendFormat(" FROM {0} ", string.Join(",", _tableParams)).Append("\r\n");
        }

        private void BuildJoinCondition()
        {
            if (_joinClause.Count > 0)
            {
                foreach (var queryJoin in _joinClause)
                {
                    _sqlBuilder.Append(queryJoin).Append("\r\n");
                }
            }

        }

        private void BuildWhereCondition()
        {
            _sqlBuilder.Append(_conditions.ToArray().ToSqlConditions());
        }

        public string ToSqlQuery()
        {
            if (_sqlBuilder.Length > 0)
            {
                return _sqlBuilder.ToString();
            }

            BuildSelect();
            BuildJoinCondition();
            BuildWhereCondition();
            
            if (_orderByParams.Count > 0)
            {
                _sqlBuilder.AppendFormat(" ORDER BY {0} ", string.Join(",", _orderByParams)).Append("\r\n");
            }

            if (_groupByParams.Count > 0)
            {
                _sqlBuilder.AppendFormat(" GROUP BY {0} ", string.Join(",", _groupByParams));
            }

            return _sqlBuilder.ToString();
        }

        public void Add<T>(IQueryJoin joinClause)
        {
            _joinClause.Add(joinClause.ToJoinQuery());
        }

        public void Add(IQueryJoin joinClause)
        {
            _joinClause.Add(joinClause.ToJoinQuery());
        }

        public void Add<T>(IJoinLinq<T> joinClause)
        {
            _joinClause.Add(joinClause.ToJoinQuery());
        }
        
        public void Add(IQueryWhere whereClause)
        {
            _filterParams.Add(whereClause);
            _conditions.AddRange(whereClause.Conditions());
        }

        public void Add(params IConditionClause[] conditions)
        {
            _conditions.AddRange(conditions);
        }

    }


}
