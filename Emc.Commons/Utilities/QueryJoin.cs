﻿using System.Text;
using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class QueryJoin : IQueryJoin
    {
        private StringBuilder _sqlJoin = new StringBuilder(100);
        private readonly IQuery _query;

        public QueryJoin(IQuery query)
        {
            _query = query;
        }

        public IQueryJoin Table(Join joinOp, string tablename)
        {
            _sqlJoin.AppendFormat(" {0} {1} ", joinOp.ToJoinOperator(), tablename);
            return this;
        }

        public IJoinLinq<T> Table<T>(Join joinOp, string tablename)
        {
            return new JoinLinq<T>( joinOp.ToJoinOperator(), tablename ,_query);
        }

        public IQueryJoin On()
        {
            _sqlJoin.Append(" ON ");
            return this;
        }

        public IQuery Compare(string leftColumn, Operator op , string rightColumn)
        {
            _sqlJoin.AppendFormat(" {0} {1} {2} \n", leftColumn, "=", rightColumn);
            _query.Add(this);
            return _query;
        }

        public string ToJoinQuery()
        {
            return _sqlJoin.ToString();
        }

    }
}
