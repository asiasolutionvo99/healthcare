﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class QueryWhere : IQueryWhere
    {
        private readonly IQuery _query;
        private readonly IUpdateQuery _updateQuery;
        private readonly IList<IConditionClause> _conditions = new List<IConditionClause>(); 
       
        public QueryWhere(IQuery query)
        {
            _query = query;
        }

        public QueryWhere(IUpdateQuery updateQuery)
        {
            _updateQuery = updateQuery;
        }

        public IQueryWhere AndIf(bool hasCondition, string column, Operator op, string value)
        {
            if (hasCondition)
            {
                return And(column, op, value);
            }

            return this;
        }

        public IQueryWhere And(string column, Operator op, string value)
        {
            var condition = string.Format(" {0} {1} {2} ", column, op.ToConditionOperator(), value);
            _conditions.Add( new ConditionClause { Conjunction = Conjunction.And, Condition = condition});
            return this;
        }

        public IQueryWhere AndIf<T>(bool hasCondition, Expression<Func<T, object>> field, Operator op, params string[] values)
        {
            if (hasCondition)
            {
                return And<T>(field, op, values);
            }

            return this;
        }

        public IQueryWhere And<T>(Expression<Func<T, object>> field, Operator op, params string[] values)
        {
            var column = field.ToSqlFieldName();
            var condition = "(1=1)";

            if (op == Operator.Between)
            {
                condition = string.Format(" {0} {1} {2} {3} ", column, op.ToConditionOperator(), values[0], values[1]);
            }
            else
            {
                
            }

            _conditions.Add(new ConditionClause { Conjunction = Conjunction.And, Condition = condition });
            return this;
        }


        public IQueryWhere Or(string column, Operator op, string value)
        {
            var condition = string.Format(" {0} {1} {2} ", column, op.ToConditionOperator(), value);
            _conditions.Add(new ConditionClause { Conjunction = Conjunction.Or, Condition = condition });

            return this;
        }

        public IQuery EndWhere()
        {
            _query.Add(this);
            return _query;
        }

        public IUpdateQuery EndWhereUpdate()
        {
            _updateQuery.Add(this);
            return _updateQuery;
        }

        public IList<IConditionClause> Conditions()
        {
            return _conditions;
        }

    }
}
