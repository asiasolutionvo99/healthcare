﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Emc.Commons.Contracts.Utilities;

namespace Emc.Commons.Utilities
{
    public class UpdateQuery : IUpdateQuery
    {
        private readonly string _tableName;

        private readonly List<DataParameter> _fieldParameters = new List<DataParameter>();
        private readonly List<string> _conditions = new List<string>();

        private readonly List<string> _ignores = new List<string>(); 

        public UpdateQuery(string tableName)
        {
            _tableName = tableName;
        }

        private void AddSetCommand<T>(Expression<Func<T, object>> field)
        {   
            var parameter = field.ToSqlParameter();
            _fieldParameters.Add( new DataParameter { Column  =  parameter, Parameter = "@" + parameter});
        }

        public IUpdateQuery SetAll<T>()
        {
            var type = typeof(T);
            var properties = type.GetProperties();

            foreach (var propertyInfo in properties)
            {
                _fieldParameters.Add(new DataParameter { Column = propertyInfo.Name, Parameter = "@" + propertyInfo.Name });
                
            }

            return this;
        }

        public void Ignore(string fieldName)
        {
            _ignores.Add(fieldName.ToLower());
        }

        public void Ignore<T>(Expression<Func<T, object>> field)
        {
            _ignores.Add(field.ToSqlFieldName().ToLower());
        }

        public IUpdateQuery Set<T>(Expression<Func<T, object>> field)
        {
            AddSetCommand(field);
            return this;
        }

        public IUpdateQuery AndSet<T>(Expression<Func<T, object>> field)
        {
            AddSetCommand(field);
            return this;
        }

        public IQueryWhere Where()
        {
            return new QueryWhere(this);
        }

        public void Add(IQueryWhere queryWhere)
        {
            if (_conditions.Any())
            {
                _conditions.Add(queryWhere.Conditions().ToArray().ToSqlConditions(false));    
            }
            else
            {
                _conditions.Add(queryWhere.Conditions().ToArray().ToSqlConditions());    
            }
        }

        public string ToSqlUpdate()
        {
            var listConds = _fieldParameters
                .Where( x => !_ignores.Contains(x.Column.ToLower()))
                .Select(x => string.Format(" {0} = {1} ", x.Column, x.Parameter))
                .ToList();

            var sqlBuilder = new StringBuilder()
                .Append(" UPDATE ").Append(_tableName).Append(" SET ").Append("\r\n")
                .Append(string.Join(",", listConds));

            if (_conditions.Any())
            {   
                foreach (var condition in _conditions)
                {
                    sqlBuilder.Append(condition);
                }
            }

            return sqlBuilder.ToString();
        }
    }

    class DataParameter
    {
        public string Column { get; set; }
        public string Parameter { get; set; }
    }
}
