﻿using System;
using System.ServiceModel;
using Castle.Facilities.WcfIntegration;
using Castle.Windsor;
using Emc.Contracts.Services;
using Emc.Services.Installers;

namespace Emc.ConsoleService
{
    class Program
    {
        private static ServiceHost _serviceHost;

        static void Main()
        {
            try
            {
                using (IWindsorContainer container = new WindsorContainer())
                {
                    container.Install(new LoggerInstaller(), new ServicesInstaller(), new UnitOfWorkInstaller());
                    var serviceHostFactory = new DefaultServiceHostFactory(container.Kernel);
                    var
                        servicehost =
                            serviceHostFactory.CreateServiceHost("Emc.Contracts.Services.IAuthenticateService",
                                new Uri[0]);

                        servicehost.Open();
                        Console.WriteLine(" Service: .... was started ");
                        Console.WriteLine(" Press any key to stop.... ");
                        Console.ReadKey();
                    
                    Console.WriteLine(" Press ENTER To exit.... ");
                    Console.ReadLine();
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                Console.WriteLine(" StackTract: " + ex.StackTrace);
                Console.ReadLine();
            }
            finally
            {
                if (_serviceHost != null)
                {
                    _serviceHost.Close();
                    _serviceHost = null;
                }
            }
        }

    }
}
