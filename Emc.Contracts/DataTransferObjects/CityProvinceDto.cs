﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains;

namespace Emc.Contracts.DataTransferObjects
{
    public class CityProvinceDto: IDataTransferObject<CityProvince>
    {
        public CityProvinceDto()
        {
            Entities = new List<CityProvince>();
            ErrorMessages = new List<ErrorMessage>();
        }
        [DataMember]
        public IList<CityProvince> Entities { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public long TotalResult { get; set; }
        [DataMember]
        public long ActionUserId { get; set; }
        [DataMember]
        public IList<ErrorMessage> ErrorMessages { get; set; }
    }
}
