﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains;

namespace Emc.Contracts.DataTransferObjects
{
    [DataContract]
    public class CountryDto: IDataTransferObject<Country>
    {
        public CountryDto()
        {
            Entities = new List<Country>();
            ErrorMessages = new List<ErrorMessage>();
        }

        [DataMember]
        public IList<Country> Entities { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public long TotalResult { get; set; }
        [DataMember]
        public long ActionUserId { get; set; }
        [DataMember]
        public IList<ErrorMessage> ErrorMessages { get; set; }
    }
}
