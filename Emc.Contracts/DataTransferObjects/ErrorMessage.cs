﻿using System.Runtime.Serialization;

namespace Emc.Contracts.DataTransferObjects
{
    [DataContract]
    public class ErrorMessage
    {
        [DataMember]
        public int ErrorNumber { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}
