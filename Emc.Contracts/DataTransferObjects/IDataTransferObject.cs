﻿using System.Collections.Generic;

namespace Emc.Contracts.DataTransferObjects
{
    public interface IDataTransferObject<T>
    {
        IList<T> Entities { get; set; } 
        int PageIndex { get; set; }
        int PageSize { get; set; }
        long TotalResult { get; set; }
        long ActionUserId { get; set; }
        IList<ErrorMessage> ErrorMessages { get; set; } 

    }
}
