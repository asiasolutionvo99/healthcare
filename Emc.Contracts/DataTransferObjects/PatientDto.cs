﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains;

namespace Emc.Contracts.DataTransferObjects
{
    [DataContract]
    public class PatientDto: IDataTransferObject<Patient>
    {
        public PatientDto()
        {
            Entities = new List<Patient>();
        }
        [DataMember]
        public IList<Patient> Entities { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public long TotalResult { get; set; }
        [DataMember]
        public long ActionUserId { get; set; }
        [DataMember]
        public IList<ErrorMessage> ErrorMessages { get; set; }
    }
}
