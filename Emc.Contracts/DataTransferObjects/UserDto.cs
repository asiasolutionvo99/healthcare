﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Emc.Domains;

namespace Emc.Contracts.DataTransferObjects
{
    [DataContract]
    public class UserDto : IDataTransferObject<User>
    {
        public UserDto()
        {
            Entities = new List<User>();
        }

        [DataMember]
        public IList<User> Entities { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int PageSize { get; set; }
        [DataMember]
        public long TotalResult { get; set; }
        [DataMember]
        public long ActionUserId { get; set; }
        [DataMember]
        public IList<ErrorMessage> ErrorMessages { get; set; }
    }
}
