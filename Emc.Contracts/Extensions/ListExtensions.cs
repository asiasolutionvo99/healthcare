﻿using System.Collections;
using System.Collections.Generic;

namespace Emc.Contracts.Extensions
{
    public static class ListExtensions
    {
        public static bool HasElements<T>(this T[] items)
        {
            return items != null && items.Length > 0;
        }

        public static bool HasElements<T>(this IList<T> items)
        {
            return items != null && items.Count > 0;
        }

        public static bool HasElements(this IList items)
        {
            return items != null && items.Count > 0;
        }

        public static bool HasElements(this ICollection items)
        {
            return items != null && items.Count > 0;
        }

    }
}
