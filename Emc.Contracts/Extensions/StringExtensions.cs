﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emc.Contracts.Extensions
{
    public static class StringExtensions
    {
        public static bool HasContent(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }
    }
}
