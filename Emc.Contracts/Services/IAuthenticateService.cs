﻿using System.ServiceModel;
using Emc.Contracts.DataContractAttributes;
using Emc.Contracts.DataTransferObjects;
using Emc.Domains.Criterias;

namespace Emc.Contracts.Services
{
    [ServiceContract]
    public interface IAuthenticateService
    {
        [OperationContract]
        [ApplyDataContractResolver]
        UserDto Authenticate(string loginname, string password);

        [OperationContract]
        [ApplyDataContractResolver]
        UserDto SaveUser(UserDto user);

        [OperationContract]
        [ApplyDataContractResolver]
        UserDto FilterUser(UserCriteria criteria);

    }
}
