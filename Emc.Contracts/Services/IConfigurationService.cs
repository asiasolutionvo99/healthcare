﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Emc.Contracts.DataContractAttributes;
using Emc.Contracts.DataTransferObjects;
using Emc.Domains;

namespace Emc.Contracts.Services
{
    [ServiceContract]
    public interface IConfigurationService
    {
        [OperationContract]
        [ApplyDataContractResolver]
        CountryDto GetAllCountry();

        [OperationContract]
        [ApplyDataContractResolver]
        CityProvinceDto GetAllCityProvince();

        [OperationContract]
        [ApplyDataContractResolver]
        SuburbDto GetAllSuburb();

        [OperationContract]
        [ApplyDataContractResolver]
        LookupDto GetAllLookup();


        [OperationContract]
        [ApplyDataContractResolver]
        PatientDto SavePatient(Patient patient);
    }
}
