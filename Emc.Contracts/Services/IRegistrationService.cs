﻿using System.ServiceModel;
using Emc.Contracts.DataContractAttributes;
using Emc.Domains;


namespace Emc.Contracts.Services
{
    [ServiceContract]
    public interface IRegistrationService
    {
        [OperationContract]
        [ApplyDataContractResolver]
        User Register(User user);
    }
}
