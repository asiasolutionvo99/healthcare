﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Emc.Contracts.DataContractAttributes;
using Emc.Contracts.DataTransferObjects;

namespace Emc.Contracts.Services
{
    [ServiceContract]
    public interface ISettingService
    {
        [OperationContract]
        [ApplyDataContractResolver]
        FieldDto SaveField(FieldDto dto);

    }
}
