﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class AppFunction : IAggregatorRoot
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string AppFunctionName { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public DateTime? CreatedTime { get; set; }
        [DataMember]
        public long? CreatedUser { get; set; }
        [DataMember]
        public DateTime? UpdatedTime { get; set; }
        [DataMember]
        public long? UpdatedUser { get; set; }
        [DataMember]
        public DateTime? DeletedTime { get; set; }
        [DataMember]
        public long? DeletedUser { get; set; }
        [DataMember]
        public virtual IList<Permission> Permissions { get; set; } 
    }
}
