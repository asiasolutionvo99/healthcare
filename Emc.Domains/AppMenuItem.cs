﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class AppMenuItem : IEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Caption { get; set; }
        [DataMember]
        public string CommandParameter { get; set; }
        [DataMember]
        public long? ParentId { get; set; }
        [DataMember]
        public virtual AppMenuItem Parent { get; set; }
        [DataMember]
        public virtual IList<AppMenuItem> Childs { get; set; } 
    }
}
