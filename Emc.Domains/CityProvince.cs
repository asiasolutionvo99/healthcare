﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class CityProvince : IEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string CityProvinceName { get; set; }
        [DataMember]
        public string CityProviceCode { get; set; }
        [DataMember]
        public string CityProviceHICode { get; set; }
        [DataMember]
        public virtual IList<Suburb> Suburbs { get; set; }
    }
}
