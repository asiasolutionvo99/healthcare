﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emc.Domains.Contracts
{
    public interface IAggregatorRoot
    {
        long Id { get; set; }
        string Comment { get; set; }
        DateTime? CreatedTime { get; set; }
        long? CreatedUser { get; set; }
        DateTime? UpdatedTime { get; set; }
        long? UpdatedUser { get; set; }
        DateTime? DeletedTime { get; set; }
        long? DeletedUser { get; set; }
    }
}
