﻿namespace Emc.Domains.Contracts
{
    public interface ICriteria 
    {
        long? EntityId { get; set; }
        string SearchText { get; set; }
        string Code { get; set; }
        bool IsDeleted { get; set; }
        int PageIndex { get; set; }
        int PageSize { get; set; }

    }
}
