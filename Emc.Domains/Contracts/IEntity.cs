﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Emc.Domains.Contracts
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
