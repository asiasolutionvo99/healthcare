﻿using Emc.Domains.Contracts;

namespace Emc.Domains.Criterias
{
    public class UserCriteria : ICriteria
    {
        public long? EntityId { get; set; }
        public string SearchText { get; set; }
        public string Code { get; set; }
        public bool IsDeleted { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public long? RoleId { get; set; }

    }
}
