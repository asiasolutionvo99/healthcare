﻿using System;
using System.Runtime.Serialization;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class Employee : IAggregatorRoot
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EmployeeName { get; set; }
        [DataMember]
        public int PositionId { get; set; }
        [DataMember]
        public DateTime? CreatedTime { get; set; }
        [DataMember]
        public long? CreatedUser { get; set; }
        [DataMember]
        public DateTime? UpdatedTime { get; set; }
        [DataMember]
        public long? UpdatedUser { get; set; }
        [DataMember]
        public DateTime? DeletedTime { get; set; }
        [DataMember]
        public long? DeletedUser { get; set; }
    }
}
