﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class Lookup:IAggregatorRoot
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long LookupCode { get; set; }
        [DataMember]
        public long ObjectTypeId { get; set; }
        [DataMember]
        public string ObjectName { get; set; }
        [DataMember]
        public string ObjectValue { get; set; }
        [DataMember]
        public string ObjectNotes { get; set; }
       
        [DataMember]
        public DateTime? CreatedTime { get; set; }
        [DataMember]
        public long? CreatedUser { get; set; }
        [DataMember]
        public DateTime? UpdatedTime { get; set; }
        [DataMember]
        public long? UpdatedUser { get; set; }
        [DataMember]
        public DateTime? DeletedTime { get; set; }
        [DataMember]
        public long? DeletedUser { get; set; }
    }
}
