﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class Patient : IAggregatorRoot
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string PatientCode { get; set; }
        [DataMember]
        public string PatientName { get; set; }
        [DataMember]
        public string Dob { get; set; }
        [DataMember]
        public string ContactSearch { get; set; }
        [DataMember]
        public long? NationalId { get; set; }
        [DataMember]
        public string NationalName { get; set; }
        [DataMember]
        public byte Gender { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public DateTime? CreatedTime { get; set; }
        [DataMember]
        public long? CreatedUser { get; set; }
        [DataMember]
        public DateTime? UpdatedTime { get; set; }
        [DataMember]
        public long? UpdatedUser { get; set; }
        [DataMember]
        public DateTime? DeletedTime { get; set; }
        [DataMember]
        public long? DeletedUser { get; set; }

        [DataMember]
        public virtual IList<PatientField> PatientFields { get; set; }     
    }
}
