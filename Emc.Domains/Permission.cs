﻿using System;
using System.Runtime.Serialization;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class Permission : IAggregatorRoot
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long? RoleId { get; set; }
        [DataMember]
        public long? AppFunctionId { get; set; }
        [DataMember]
        public bool CanView { get; set; }
        [DataMember]
        public bool CanCreate { get; set; }
        [DataMember]
        public bool CanEdit { get; set; }
        [DataMember]
        public bool CanDelete { get; set; }
        [DataMember]
        public bool CanPrint { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public DateTime? CreatedTime { get; set; }
        [DataMember]
        public long? CreatedUser { get; set; }
        [DataMember]
        public DateTime? UpdatedTime { get; set; }
        [DataMember]
        public long? UpdatedUser { get; set; }
        [DataMember]
        public DateTime? DeletedTime { get; set; }
        [DataMember]
        public long? DeletedUser { get; set; }
        [DataMember]
        public virtual AppFunction AppFunction { get; set; }
        [DataMember]
        public virtual Role Role { get; set; }

    }
}
