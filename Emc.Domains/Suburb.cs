﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class Suburb : IEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long CityProvinceId { get; set; }
        
        [DataMember]
        public string SuburbName { get; set; }
        [DataMember]
        public string DescNote { get; set; }
    }
}
