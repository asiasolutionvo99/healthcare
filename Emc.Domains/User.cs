﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Emc.Domains.Contracts;

namespace Emc.Domains
{
    [DataContract]
    public class User : IAggregatorRoot
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string LoginName { get; set; }
        [DataMember]
        public string Pwd { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public DateTime? CreatedTime { get; set; }
        [DataMember]
        public long? CreatedUser { get; set; }
        [DataMember]
        public DateTime? UpdatedTime { get; set; }
        [DataMember]
        public long? UpdatedUser { get; set; }
        [DataMember]
        public DateTime? DeletedTime { get; set; }
        [DataMember]
        public long? DeletedUser { get; set; }
        [DataMember]
        public long RoleId { get; set; }
        [DataMember]
        public Role Role { get; set; }

    }
}
