﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class AppFunctionMapping : EntityTypeConfiguration<AppFunction>
    {
        public AppFunctionMapping()
        {
            ToTable("AppFunctions");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("AppFunctionId");
            HasMany(x => x.Permissions).WithRequired(x => x.AppFunction).WillCascadeOnDelete();
        }
    }
}
