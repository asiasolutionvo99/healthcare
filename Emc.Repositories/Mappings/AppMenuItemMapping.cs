﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class AppMenuItemMapping : EntityTypeConfiguration<AppMenuItem>
    {
        public AppMenuItemMapping()
        {
            ToTable("AppMenuItems");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("AppMenuItemId");
            HasOptional(x => x.Parent);
            HasMany(x => x.Childs).WithOptional(x => x.Parent).WillCascadeOnDelete();
        }
    }
}
