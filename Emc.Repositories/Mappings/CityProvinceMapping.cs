﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class CityProvinceMapping: EntityTypeConfiguration<CityProvince>
    {
        public CityProvinceMapping()
        {
            ToTable("CityProvinces");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("CityProvinceId");
            HasMany(x => x.Suburbs);
        }
    }
}
