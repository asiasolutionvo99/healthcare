﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class CountryMapping:EntityTypeConfiguration<Country>
    {
        public CountryMapping()
        {
            ToTable("Countries");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("CountryId");
        }
    }
}
