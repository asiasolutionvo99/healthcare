﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class FieldMapping : EntityTypeConfiguration<Field>
    {
        public FieldMapping()
        {
            ToTable("Fields");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("FieldId");
            Property(x => x.FieldName).HasMaxLength(200);
            Property(x => x.DataType).HasMaxLength(200);

            HasMany(x => x.PatientFields);
        }
    }
}
