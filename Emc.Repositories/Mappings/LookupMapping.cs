﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class LookupMapping: EntityTypeConfiguration<Lookup>
    {
        public LookupMapping()
        {
            ToTable("Lookups");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("LookupId");

            
        }
    }
}
