﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{

    public class PatientFieldMapping : EntityTypeConfiguration<PatientField>
    {
        public PatientFieldMapping()
        {
            ToTable("PatientFields");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("PatientFieldId");
            //HasRequired(x => x.Field);
            //HasRequired(x => x.Patient);

        }
    }
}
