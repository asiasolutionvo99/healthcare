﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class PatientMapping : EntityTypeConfiguration<Patient>
    {
        public PatientMapping()
        {
            ToTable("Patients");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("PatientId");
            Property(x => x.PatientCode).HasMaxLength(10);
            Property(x => x.PatientName).HasMaxLength(200);
            Property(x => x.Dob).HasMaxLength(10);
            Property(x => x.Gender);
            
            HasMany(x => x.PatientFields);

        }
    }
}
