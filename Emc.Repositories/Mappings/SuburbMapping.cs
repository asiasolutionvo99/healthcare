﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class SuburbMapping: EntityTypeConfiguration<Suburb>
    {
        public SuburbMapping()
        {
            ToTable("Suburbs");
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("SuburbId");
            
        }
    }
}
