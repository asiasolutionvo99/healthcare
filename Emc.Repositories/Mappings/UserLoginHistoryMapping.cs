﻿using System.Data.Entity.ModelConfiguration;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class UserLoginHistoryMapping: EntityTypeConfiguration<UserLoginHistory>
    {
        public UserLoginHistoryMapping()
        {
            ToTable("UserLoginHistories");
            HasKey(x=>x.Id);
            Property(x => x.Id).HasColumnName("UserLoginHistoryId");

        }
    }
}
