﻿using Emc.Commons.Contracts;
using Emc.Commons.Utilities;
using Emc.Domains;

namespace Emc.Repositories.Mappings
{
    public class UserMapping : IDbInfo<User>
    {
        public string TableName { get { return "Users"; }}
        public string PrimaryKey { get { return "UserId"; } }

        public string InsertQuery()
        {
            var query = Query.InsertInto(TableName)
                .Fields<User>()
                .IgnoreField("Id")
                .IgnoreField("Role").ToSqlInsert();

            return query;
        }

        public string UpdateQuery()
        {
            var query = Query.Update(TableName)
                .Set<User>( x => x.LoginName )
                .AndSet<User>(x => x.Pwd)
                .AndSet<User>(x => x.RoleId).ToSqlUpdate();

            return query;
        }

        public string DeleteQuery()
        {
            var query = string.Format(" DELETE FROM {0} WHERE {1} = @Id ", TableName, PrimaryKey);
            return query;
        }
    }
}
