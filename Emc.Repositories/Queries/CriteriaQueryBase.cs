﻿using Emc.Commons.Contracts.Utilities;
using Emc.Commons.Utilities;
using Emc.Domains.Contracts;

namespace Emc.Repositories.Queries
{
    public static class CriteriaQueryBase
    {
        public static IQuery ToCriteriaQuery(this ICriteria criteria)
        {
            var query = Query.WithBuilder();

            query.BeginWhere()
                .AndIf( criteria.EntityId.HasValue, "Id", Operator.Equals, "@Id" )
                .AndIf(criteria.IsDeleted, "IsRetired", Operator.Equals, "1")
                .EndWhere();
           
            return query;
        }
    }
}
