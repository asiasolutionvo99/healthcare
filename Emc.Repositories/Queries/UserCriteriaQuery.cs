﻿using Emc.Commons.Contracts.Utilities;
using Emc.Contracts.Extensions;
using Emc.Domains;
using Emc.Domains.Criterias;

namespace Emc.Repositories.Queries
{
    public static class UserCriteriaQuery
    {
        public static IQuery ToLoginQuery(this UserCriteria criteria)
        {
            var query = criteria.ToCriteriaQuery();
            query.Select<User>();
            query.BeginWhere()
                .AndIf( criteria.SearchText.HasContent(), "LoginName", Operator.Like,"@SearchText")
                .AndIf(criteria.RoleId.HasValue, "RoleId", Operator.Equals, "@RoleId");

            return query;
        }
    }
}
