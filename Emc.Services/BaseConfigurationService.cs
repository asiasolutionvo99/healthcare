﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Castle.Windsor;
using Emc.Commons.Contracts;
using Emc.Contracts.DataTransferObjects;
using Emc.Contracts.Services;
using Emc.Domains;
using Emc.Services.Helpers;

namespace Emc.Services
{
    public class BaseConfigurationService: BaseService, IConfigurationService
    {
        public BaseConfigurationService(IWindsorContainer container) : base(container)
        {
        }

        public CountryDto GetAllCountry()
        {
            var result = new CountryDto();
            try
            {
                var totalRecord = 0;
                using (var unitOfWork = Container.Resolve<IUnitOfWork>())
                {
                    var countryRep = unitOfWork.Repository<Country>();
                    var countries = countryRep.Query().ToArray();
                    result.Entities = countries; 
                }
            }
            catch (SystemException ex)
            {
                result.ErrorMessages = Logger.ExceptionHandler(ex);
            }
            return result;
        }



        public CityProvinceDto GetAllCityProvince()
        {
            var result = new CityProvinceDto();
            try
            {
                var totalRecord = 0;
                using (var unitOfWork = Container.Resolve<IUnitOfWork>())
                {
                    var provinceRep = unitOfWork.Repository<CityProvince >();
                    var province = provinceRep.Query().OrderBy(x => x.CityProvinceName).Include(x => x.Suburbs).ToArray();
                    result.Entities =  province ;
                }
            }
            catch (SystemException ex)
            {
                result.ErrorMessages = Logger.ExceptionHandler(ex);
            }
            return result;
        }

        public SuburbDto GetAllSuburb()
        {

            var result = new SuburbDto() ;
            try
            {
                var totalRecord = 0;
                using (var unitOfWork = Container.Resolve<IUnitOfWork>())
                {
                    var suburbRep = unitOfWork.Repository<Suburb >();
                    var suburbs = suburbRep.Query().ToArray();
                    result.Entities = suburbs;
                }
            }
            catch (SystemException ex)
            {
                result.ErrorMessages = Logger.ExceptionHandler(ex);
            }
            return result;
        }

        public LookupDto GetAllLookup()
        {
            throw new NotImplementedException();
        }

        public PatientDto SavePatient(Patient patient)
        {
            var result = new PatientDto();
            try
            {
                var totalRecord = 0;
                using (var unitOfWork = Container.Resolve<IUnitOfWork>())
                {
                    var patientRep = unitOfWork.Repository<Patient>();
                    patientRep.Save(patient);
                    unitOfWork.SaveChanges();
                    result.Entities.Add(patient);
                }
            }
            catch (SystemException ex)
            {
                result.ErrorMessages = Logger.ExceptionHandler(ex);
            }
            return result;
        }
    }
}
