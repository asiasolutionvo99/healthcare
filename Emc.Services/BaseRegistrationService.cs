﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emc.Contracts.Services;
using Emc.Domains;

namespace Emc.Services
{
    public class BaseRegistrationService : IRegistrationService
    {
        public User Register(User user)
        {
            user.RoleId = 1;

            return user;
        }
    }
}
