﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Castle.Core.Logging;
using Castle.Windsor;
using Emc.Commons.BaseRepository;
using Emc.Commons.Contracts;
using Emc.Domains.Contracts;

namespace Emc.Services
{
    public abstract class BaseService
    {
        protected IWindsorContainer Container { get; private set; }
        protected ILogger Logger { get; private set; }

        protected BaseService(IWindsorContainer container)
        {
            Container = container;
            Logger = Container.Resolve<ILogger>();
        }

        protected void Save<T>(IUnitOfWork unitOfWork, long userActionId, params T[] entities)
            where T : class, IAggregatorRoot
        {

            var rep = unitOfWork.Repository<T>();
            foreach (var entity in entities)
            {
                if (entity.Id > 0)
                {
                    entity.UpdatedTime = DateTime.Now;
                    entity.UpdatedUser = userActionId;
                }
                else
                {
                    entity.CreatedTime = DateTime.Now;
                    entity.CreatedUser = userActionId;
                }

                rep.Save(entity);
            }

        }

        protected void Retired<T>(IUnitOfWork unitOfWork, long userActionId, params T[] entities)
            where T : class, IAggregatorRoot
        {
            var rep = unitOfWork.Repository<T>();
            foreach (var entity in entities)
            {
                entity.DeletedTime = DateTime.Now;
                entity.DeletedUser = userActionId;
                rep.Save(entity);
            }

        }

        protected void Remove<T>(IUnitOfWork unitOfWork, long userActionId, params T[] entities)
            where T : class, IAggregatorRoot
        {
            var rep = unitOfWork.Repository<T>();
            foreach (var entity in entities)
            {
                Logger.InfoFormat(" DELETED: UserId {0} Delete {1} ", userActionId, entity.Id);
                rep.DeleteById(entity.Id);
            }

        }

        protected void RemoveById<T>(IUnitOfWork unitOfWork, long userActionId, params long[] ids)
           where T : class, IAggregatorRoot
        {
            var rep = unitOfWork.Repository<T>();
            foreach (var entity in ids)
            {
                Logger.InfoFormat(" DELETED: UserId {0} Delete {1} ", userActionId, entity);
                rep.DeleteById(entity);
            }

        }

        protected IList<T> BuildFilter<T>(
            CriteriaQuery<T> criteriaQuery
            , ICriteria criteria
            , out int totalResults
            , List<Expression<Func<T, object>>> includeProperties = null
            ) where T : class
        {
            var unitOfWork = Container.Resolve<IUnitOfWork>();
            if (includeProperties != null)
            {
                return BuildFilter<T>(unitOfWork, criteriaQuery, criteria, out totalResults, includeProperties.ToArray());
            }

            return BuildFilter<T>(unitOfWork, criteriaQuery, criteria, out totalResults);
        }

        protected IList<T> BuildFilter<T>(IUnitOfWork unitOfWork,
            CriteriaQuery<T> criteriaQuery
            , ICriteria criteria
            , out int totalResults
            , params Expression<Func<T, object>>[] includeProperties
            ) where T : class
        {
            var rep = unitOfWork.Repository<T>();

            var query = rep.Query();

            var expression = criteriaQuery.AsExpression();

            totalResults = query.Count(expression);

            if (includeProperties != null && includeProperties.Length > 0)
            {
                foreach (var includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }

            if (criteria.PageIndex > 0 && criteria.PageSize > 0)
            {
                var skip = (criteria.PageIndex - 1) * criteria.PageSize;
                query = query.Skip(skip).Take(criteria.PageSize);
            }

            return query.Where(expression).ToList();
        }
    }
}
