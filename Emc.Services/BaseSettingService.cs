﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Windsor;
using Emc.Commons.Contracts;
using Emc.Contracts.DataTransferObjects;
using Emc.Contracts.Services;
using Emc.Domains;
using Emc.Services.Helpers;

namespace Emc.Services
{
    public class BaseSettingService : BaseService , ISettingService
    {
        public BaseSettingService(IWindsorContainer container) : base(container)
        {
        }


        public FieldDto SaveField(FieldDto dto)
        {
            var result = new FieldDto();

            try
            {
                if (dto == null)
                {
                    throw new ArgumentNullException("dto", " Field Dto is null ");
                }

                var totalRecords = 0;

                using (var unitOfWork = Container.Resolve<IUnitOfWork>())
                {
                    Save(unitOfWork, dto.ActionUserId, dto.Entities.ToArray());
                    totalRecords = unitOfWork.SaveChanges();
                }

                result.Entities = dto.Entities;
                result.TotalResult = totalRecords;

            }
            catch (SystemException ex)
            {
                result.ErrorMessages = Logger.ExceptionHandler(ex);
            }

            return result;
        }
    }
}
