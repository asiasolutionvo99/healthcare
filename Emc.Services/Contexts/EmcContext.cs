﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Castle.Windsor;
using Emc.Commons.Contracts;
using Emc.Domains.Contracts;

namespace Emc.Services.Contexts
{
    public class EmcContext : IDataContext
    {
        private readonly IWindsorContainer _container;
        private IDbConnection _connection;

        public EmcContext(IWindsorContainer container)
        {
            _container = container;
        }

        public IDbConnection Connection
        {
            get
            {
                if (CurrenTransaction != null 
                    &&
                    CurrenTransaction.Connection.State != ConnectionState.Closed)
                {
                    _connection = CurrenTransaction.Connection;
                    
                }

                _connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Emc"].ConnectionString);
                return _connection;
            }
        }

        public IDbInfo<T> Db<T>() where T : class, IAggregatorRoot
        {
            return _container.Resolve<IDbInfo<T>>();
        }

        public IDbTransaction CurrenTransaction { get; set; }

        public void Dispose()
        {
            if (CurrenTransaction != null)
            {
                CurrenTransaction.Dispose();
            }

            if (_connection != null)
            {
                _connection.Dispose();
            }

            GC.SuppressFinalize(this);
        }
    }
}
