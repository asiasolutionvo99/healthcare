﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Emc.Commons.Contracts;
using Emc.Domains.Contracts;

namespace Emc.Services.Data
{
    public class EmcDataContext: IDataContext
    {
        private static string _connectionString;

        public EmcDataContext()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["Emc"].ConnectionString;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public IDbConnection Connection { get { return new SqlConnection(_connectionString); } }
        public IDbTransaction CurrenTransaction { get; set; }

        public IDbInfo<T> Db<T>() where T : class, IAggregatorRoot
        {
            throw new System.NotImplementedException();
        }
    }
}
