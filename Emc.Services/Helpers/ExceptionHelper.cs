﻿using System;
using System.Collections.Generic;
using Castle.Core.Logging;
using Castle.Windsor;
using Emc.Contracts.DataTransferObjects;

namespace Emc.Services.Helpers
{
    public static class ExceptionHelper
    {
        public static IList<ErrorMessage> ExceptionHandler(this ILogger logger, Exception ex)
        {   
            logger.Error(ex.Message, ex);
            return new[] {new ErrorMessage {ErrorNumber = 50001, Message = ex.Message},};
        }
    }
}
