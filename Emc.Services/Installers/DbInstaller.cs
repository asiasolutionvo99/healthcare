﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Emc.Commons.Contracts;
using Emc.Domains;
using Emc.Repositories.Mappings;

namespace Emc.Services.Installers
{
    public class DbInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDbInfo<User>>().ImplementedBy<UserMapping>());
        }
    }
}
