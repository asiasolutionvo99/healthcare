﻿using System;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Emc.Contracts.Services;

namespace Emc.Services.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<WcfFacility>(f => f.CloseTimeout = TimeSpan.Zero)
                .Register(Component.For<IAuthenticateService>()
                    .ImplementedBy<BaseAuthenticateService>().Named("Emc.Contracts.Services.IAuthenticateService")
                );
        }
    }
}
