﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Emc.Commons.BaseRepository;
using Emc.Commons.Contracts;
using Emc.Services.Contexts;

namespace Emc.Services.Installers
{
    public class UnitOfWorkInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IDataContext>().ImplementedBy<EmcContext>().LifestylePerThread());
            container.Register(Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestylePerThread());

        }
    }
}
