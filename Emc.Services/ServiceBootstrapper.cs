﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Emc.Contracts.Services;
using Emc.Services.Installers;

namespace Emc.Services
{
    public class ServiceBootstrapper
    {
        private readonly IWindsorContainer _container = new WindsorContainer();

        public ServiceBootstrapper()
        {
            RegisterBindings();
            //RegisterServices();
        }

        public IWindsorContainer Container { get { return _container; } }

        private void RegisterBindings()
        {
            _container.Register(Component.For<IWindsorContainer>().Instance(_container));
            _container.Install(new LoggerInstaller());
            _container.Install(new UnitOfWorkInstaller());
        }

        private void RegisterServices()
        {
            _container.Register(Component.For<IAuthenticateService>().ImplementedBy<BaseAuthenticateService>().LifestyleSingleton());
            
        }
    }
}
