﻿using System;
using Emc.ClientCommons.ServiceClients;
using Emc.Contracts.Services;
using Emc.Domains;

namespace ServiceClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IServiceClientFactory<IRegistrationService> serviceClientFactory =
                new ServiceClientFactory<IRegistrationService>();

            var proxy = serviceClientFactory.GetServiceClient();

            var user = new User {Id = 1, RoleId = 0};
            var result = proxy.Register(user);

            Console.WriteLine(" Role Id: " + result.RoleId);

            Console.ReadLine();
        }
    }
}
