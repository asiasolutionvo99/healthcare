﻿using System;
using System.Data.Entity;
using System.Linq;
using Castle.Windsor;
using Emc.Commons.BaseRepository;
using Emc.Commons.Contracts;
using Emc.Domains;
using Emc.Domains.Criterias;
using Emc.Repositories.Queries;
using Emc.Services.Installers;
using NUnit.Framework;

namespace UnitTestBusinessRules
{
    [TestFixture]
    public class TestSaveExport
    {
        private readonly IWindsorContainer Container = new WindsorContainer();

        [SetUp]
        public void SetUpContainer()
        {
            Container.Install(
                new LoggerInstaller(),
                new ServicesInstaller(),
                new UnitOfWorkInstaller()
                );
        }

        [Test]
        public void TestInsertUserData()
        {
           
            var user = new User {LoginName = "admin", Pwd = "a", RoleId = 1, CreatedTime = DateTime.Now, CreatedUser = 1};

            using (var unitOfWork = Container.Resolve<IUnitOfWork>())
            {   
                var roleRepository = unitOfWork.Repository<Role>();
                var userRepository = unitOfWork.Repository<User>();
                userRepository.Save(user);
                roleRepository.Save(new Role());
                unitOfWork.SaveChanges();
            }

            Assert.That(user.Id, Is.GreaterThan(0));
        }

        [Test]
        public void TestUpdateUserData()
        {
            var user = new User { Id = 1, LoginName = "admin", Pwd = "aaa", RoleId = 1, 
                CreatedTime = DateTime.Now, CreatedUser = 1 
                , UpdatedTime = DateTime.Now, UpdatedUser = 1

            };

            var result = 0;

            using (var unitOfWork = Container.Resolve<IUnitOfWork>())
            {
                var userRepository = unitOfWork.Repository<User>();
                userRepository.Save(user);
                result = unitOfWork.SaveChanges();
            }

            Assert.That(result, Is.GreaterThan(0));
        }

        [Test]
        public void TestFilterUserData()
        {
            var criteria = new UserCriteria
            {
                SearchText = "a"
            };

            var result = 0;
            using (var unitOfWork = Container.Resolve<IUnitOfWork>())
            {
                var query = criteria.ToQuery<User>();
                var userRep = unitOfWork.Repository<User>();

                var enumUsers = userRep.Query().Include(x => x.Role).Where(query.AsExpression());

                result = enumUsers.Count();

                foreach (var enumUser in enumUsers)
                {
                    Console.WriteLine(enumUser.Role.RoleName);
                }

            }

            Assert.That(result, Is.GreaterThan(0));
        }
    }
}
